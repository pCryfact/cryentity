﻿using CryEntity;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    class TestDatabase : ICryEntityDataBase
    {
        public DbConnection CreateConnection()
        {
            return new MySqlConnection("server=localhost;user=root;database=test;port=3306;password=1234567");
        }
    }

}
