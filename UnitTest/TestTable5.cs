﻿using CryEntity;

namespace UnitTest
{
    [CryEntityTableName("testtable5")]
    public class TestTable5
    {
        [CryEntityColumnName("PrimaryKey")]
        [CryEntityPrimaryKey]
        [CryEntityAutoIncrement]
        public int PrimaryKey { get; set; }

    }
}
