﻿using CryEntity;

namespace UnitTest
{
    [CryEntityTableName("testtable4")]
    public class TestTable4
    {
        [CryEntityColumnName("PrimaryKey")]
        [CryEntityPrimaryKey]
        [CryEntityAutoIncrement]
        public int PrimaryKey { get; set; }
    }
}
