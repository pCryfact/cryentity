﻿using CryEntity;

namespace UnitTest
{
    [CryEntityTableName("testtable2")]
    [CryEntityForeignKey("TestForeign", CryEntityJoinType.Left)] // is nullable
    public class TestTable2 : TestTable1
    {
        [CryEntityPrimaryKey]
        [CryEntityColumnName("TestPrimary2")]
        [CryEntityAutoIncrement]
        public int TestPrimary2 { get; set; }

        [CryEntityColumnName("TestForeign")]
        public int TestForeign { get; set; }
    }
}
