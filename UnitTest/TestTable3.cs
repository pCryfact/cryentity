﻿using System.Collections.Generic;
using CryEntity;

namespace UnitTest
{
    [CryEntityTableName("testtable3")]
    public class TestTable3
    {
        [CryEntityColumnName("PrimaryKey")]
        [CryEntityPrimaryKey]
        [CryEntityAutoIncrement]
        public int PrimaryKey { get; set; }

        [CryEntityForeignKey("Test2", CryEntityJoinType.Left)] // is nullable
        public TestTable2 Test { get; set; }

        [CryEntityForeignKey("Table3", CryEntityJoinType.Left)] // is nullable
        public ICollection<TestTable4> Table3 { get; set; }

        [CryEntityForeignKey("Table5", CryEntityJoinType.Left)] // is nullable
        public IDictionary<int, TestTable5> Table4 { get; set; }
    }
}
