﻿using CryEntity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class CryEntityTestQueryable
    {
        [TestMethod]
        public void TestSelect()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select x).Take(3);

            int currValue = 5;
            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestSelectSingleColumn()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select x.TestString).Take(3);

            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual($"Test{strValue++}", entity, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestSelectMultiColumn()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select new { x.TestString, x.Value }).Take(3);

            int currValue = 5;
            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestWhere()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() where x.TestFloat == 1.0f && x.TestDecimal == 1.0m select x).ToList();

            Assert.AreEqual(1, entities.Count, "Failed to fill TestTable1 Entity");

            foreach (var entity in entities)
            {
                Assert.AreEqual(6, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual("Test2", entity.TestString, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestOrderByAsc()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() orderby x.TestString select x).Take(3);

            int currValue = 5;
            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestOrderByDesc()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() orderby x.TestString descending select x).Take(3);

            int currValue = 7;
            int strValue = 3;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue--, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue--}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestMultiOrderByMixed()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() orderby x.Value descending orderby x.TestDecimal select x).Take(3);

            int[] currValue = { 7, 5, 6 };
            int[] strValue = { 3, 1, 2 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestTableGroupBy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() group x by x.TestDate into grp select grp).Take(3);
            
            int[] currValue = { 5, 7, 6 };
            int[] strValue = { 1, 3, 2 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var groupedEntity in entities)
            {
                foreach (var entity in groupedEntity)
                {
                    Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                    Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                }
            }
        }

        [TestMethod]
        public void TestGroupBy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() group x by x.TestDate into grp select new { TestDate = grp.Key, Count = grp.Count() }).Take(3);
            int Count = 2;

            foreach (var b in entities)
            {
                Assert.AreEqual(Count--, b.Count, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestGroupByWithHaving()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() group x by x.TestDate into grp where grp.Count() > 1 select grp).Take(3);

            int[] currValue = { 5, 7 };
            int[] strValue = { 1, 3 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var groupedEntity in entities)
            {
                foreach (var entity in groupedEntity)
                {
                    Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                    Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                }
            }
        }

        [TestMethod]
        public void TestMultiGroupByWithHaving()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() group x by new { x.TestDate, x.TestFloat } into grp where grp.Count() > 1 select grp).Take(3);

            int[] currValue = { 5, 7 };
            int[] strValue = { 1, 3 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var groupedEntity in entities)
            {
                foreach (var entity in groupedEntity)
                {
                    Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                    Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                }
            }
        }

        [TestMethod]
        public void TestJoin()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() join y in entityManager.Table<TestTable2>() on x.Value equals y.TestForeign select new { x, y }).Take(3);
            int currValue = 5;
            int strValue = 1;

            int[] prim = { 7, 3, 2 };

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue, entity.x.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue}", entity.x.TestString, "Failed to fill TestTable1 Entity");

                Assert.AreEqual(currValue++, entity.y.TestForeign, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(prim[strValue - 1], entity.y.TestPrimary2, "Failed to fill TestTable2 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.y.TestString, "Failed to fill TestTable2 Entity");
            }
        }

        [TestMethod]
        public void TestJoinWhereOrderBy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() join y in entityManager.Table<TestTable2>() on x.Value equals y.TestForeign where x.TestFloat == 1.0f orderby x.Value select new { x, y }).Take(3);

            int currValue = 5;
            int strValue = 1;

            int[] prim = { 7, 3, 2 };

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue, entity.x.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue}", entity.x.TestString, "Failed to fill TestTable1 Entity");

                Assert.AreEqual(currValue++, entity.y.TestForeign, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(prim[strValue - 1], entity.y.TestPrimary2, "Failed to fill TestTable2 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.y.TestString, "Failed to fill TestTable2 Entity");
            }
        }

        [TestMethod]
        public void TestCount()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var count = entityManager.Table<TestTable1>().Take(3).Count();

            Assert.AreEqual(3, count, "Failed to get count");
        }

        [TestMethod]
        public void TestAverage()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var avg = (from x in entityManager.Table<TestTable1>() select x.TestDecimal).Take(3).Average();

            Assert.AreEqual(0.77558979412645866666667m, avg, "Failed to get average");
        }

        [TestMethod]
        public void TestMin()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var min = (from x in entityManager.Table<TestTable1>() select x.TestDecimal).Take(3).Min();

            Assert.AreEqual(0.4067512300828240000m, min, "Failed to get min");
        }

        [TestMethod]
        public void TestMax()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var max = (from x in entityManager.Table<TestTable1>() select x.TestDecimal).Take(3).Max();

            Assert.AreEqual(1.0m, max, "Failed to get max");
        }

        [TestMethod]
        public void TestSum()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var sum = (from x in entityManager.Table<TestTable1>() select x.TestDecimal).Take(3).Sum();

            Assert.AreEqual(2.3267693823793760000m, sum, "Failed to get max");
        }

        [TestMethod]
        public void TestFirst()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = (from x in entityManager.Table<TestTable1>() select x).First();

            Assert.AreEqual(5, entity.Value, "Failed to get entity");
        }

        [TestMethod]
        public void TestLast()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = (from x in entityManager.Table<TestTable1>() select x).Take(3).Last();

            Assert.AreEqual(7, entity.Value, "Failed to get entity");
        }

        [TestMethod]
        public void TestSkip()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select x).Take(3).Skip(1).ToList();

            Assert.AreEqual(2, entities.Count, "Failed to fill TestTable1 Entity");

            int currValue = 6;
            int strValue = 2;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestTake()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select x).Take(2).ToList();

            Assert.AreEqual(2, entities.Count, "Failed to fill TestTable1 Entity");

            int currValue = 5;
            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestSkipLast()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());
            var entities = (from x in entityManager.Table<TestTable1>() select x).Take(3).SkipLast(2).ToList();

            Assert.AreEqual(1, entities.Count, "Failed to fill TestTable1 Entity");

            int currValue = 5;
            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestTakeLast()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select x).Take(3).TakeLast(2).ToList();

            Assert.AreEqual(2, entities.Count, "Failed to fill TestTable1 Entity");

            int currValue = 5;
            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestUnion()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() where x.Value == 5 select x).Union(from x in entityManager.Table<TestTable1>() where x.Value == 7 select x).ToList();

            Assert.AreEqual(2, entities.Count, "Failed to fill TestTable1 Entity");

            int[] currValue = { 5, 7 };
            int[] strValue = { 1, 3 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestUnionAll()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() where x.Value == 5 select x).Concat(from x in entityManager.Table<TestTable1>() where x.Value < 7 select x).ToList();

            Assert.AreEqual(3, entities.Count, "Failed to fill TestTable1 Entity");

            int[] currValue = { 5, 5, 6 };
            int[] strValue = { 1, 1, 2 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestExcept()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = (from x in entityManager.Table<TestTable1>() select x).Take(2).Except(from x in entityManager.Table<TestTable1>() where x.Value == 7 select x).ToList();

            Assert.AreEqual(2, entities.Count, "Failed to fill TestTable1 Entity");

            int[] currValue = { 5, 6 };
            int[] strValue = { 1, 2 };
            int currValueIdx = 0;
            int strValueIdx = 0;

            foreach (var entity in entities)
            {
                Assert.AreEqual(currValue[currValueIdx++], entity.Value, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"Test{strValue[strValueIdx++]}", entity.TestString, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable1 Entity");
                Assert.AreEqual(1, entity.TestFloat, "Failed to fill TestTable1 Entity");
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////// LAZY TESTS //////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////
        
        [TestMethod]
        public void TestSelectLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = from x in entityManager.TableLazy<TestTable6>() select x;

            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(strValue, entity.PrimaryKey, "Failed to fill TestTable6 Entity");
                Assert.AreEqual($"{strValue++}", entity.TestString, "Failed to fill TestTable6 Entity");
            }

            Assert.AreEqual(500, strValue, "Failed to fill TestTable6 Entity");
        }

        [TestMethod]
        public void TestSelectSingleColumnLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = from x in entityManager.TableLazy<TestTable6>() select x.TestString;

            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual($"{strValue++}", entity, "Failed to fill TestTable6 Entity");
            }

            Assert.AreEqual(500, strValue, "Failed to fill TestTable6 Entity");
        }

        [TestMethod]
        public void TestSelectMultiColumnLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = from x in entityManager.TableLazy<TestTable6>() select new { x.TestString, x.PrimaryKey };

            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(strValue, entity.PrimaryKey, "Failed to fill TestTable6 Entity");
                Assert.AreEqual($"{strValue++}", entity.TestString, "Failed to fill TestTable6 Entity");
            }

            Assert.AreEqual(500, strValue, "Failed to fill TestTable6 Entity");
        }

        [TestMethod]
        public void TestWhereLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = from x in entityManager.TableLazy<TestTable6>() where x.PrimaryKey > 1 select x;

            int strValue = 2;

            foreach (var entity in entities)
            {
                Assert.AreEqual(strValue, entity.PrimaryKey, "Failed to fill TestTable6 Entity");
                Assert.AreEqual($"{strValue++}", entity.TestString, "Failed to fill TestTable6 Entity");
            }

            Assert.AreEqual(500, strValue, "Failed to fill TestTable6 Entity");
        }
    }
}
