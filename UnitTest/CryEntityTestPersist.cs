﻿using CryEntity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class CryEntityTestPersist
    {
        [TestMethod]
        public void TestPersistNormal()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = new TestTable1();

            var testValue = (decimal)new Random().NextDouble();

            entity.TestDecimal = testValue;
            entity.TestString = string.Empty;

            int id = (int)Convert.ChangeType(entityManager.Persist(entity), typeof(int));

            entity = entityManager.Find<TestTable1>(id).First();

            Assert.AreEqual(testValue, entity.TestDecimal, "Failed to store TestTable1 Entity");
        }

        [TestMethod]
        public void TestPersistInheritance()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = new TestTable2();

            var testValue = (decimal)new Random().NextDouble();

            entity.TestDecimal = testValue;
            entity.TestString = string.Empty;

            int id = (int)Convert.ChangeType(entityManager.Persist(entity), typeof(int));

            entity = entityManager.Find<TestTable2>(id).First();

            Assert.AreEqual(testValue, entity.TestDecimal, "Failed to store TestTable2 Entity");
        }

        [TestMethod]
        public void TestPersistInheritanceAndPropertyWithEntity()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = new TestTable3();
            entity.Test = new TestTable2();
            entity.Table3 = new List<TestTable4>();
            entity.Table4 = new Dictionary<int, TestTable5>();

            var testValue = (decimal)new Random().NextDouble();

            entity.Test.TestDecimal = testValue;
            entity.Test.TestString = string.Empty;

            int id = (int)Convert.ChangeType(entityManager.Persist(entity), typeof(int));

            entity = entityManager.Find<TestTable3>(id).First();

            Assert.AreEqual(testValue, entity.Test.TestDecimal, "Failed to store TestTable1 Entity");
        }
    }
}
