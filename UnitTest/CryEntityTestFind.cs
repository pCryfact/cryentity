using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryEntity;
using System;

namespace UnitTest
{
    [TestClass]
    public class CryEntityTestFind
    {
        [TestMethod]
        public void TestFindNormal()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = entityManager.Find<TestTable1>();

            int currValue = 6;
            int strValue = 1;

            foreach (var entity in entities)
            {
                if (strValue == 1)
                {
                    ++strValue;
                    continue;
                }

                if (strValue == 3)
                    break;

                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable2 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(1, entity.TestDecimal, "Failed to fill TestTable2 Entity");
            }
        }

        [TestMethod]
        public void TestFindInheritance()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = entityManager.Find<TestTable2>();

            int currValue = 5;
            int strValue = 2;
            int currPrimaryIdx = 0;
            int[] currPrimary = new int[] { 3, 2 };

            foreach (var entity in entities)
            {
                if (currValue == 5)
                    continue;

                Assert.AreEqual(currPrimary[currPrimaryIdx++], entity.TestPrimary2, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable2 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(1, entity.TestDecimal, "Failed to fill TestTable2 Entity");
            }
        }

        [TestMethod]
        public void TestFindInheritanceAndPropertyWithEntity()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = entityManager.Find<TestTable3>();

            int value = 1;

            foreach (var entity in entities)
            {
                if (entity.Table3 == null || entity.Table4 == null)
                    continue;

                Assert.AreEqual(4, entity.Table3.Count, "Failed to fill TestTable3 Entity");
                Assert.AreEqual(2, entity.Table4.Count, "Failed to fill TestTable3 Entity");
                Assert.AreEqual(value++, entity.PrimaryKey, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestFindLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = entityManager.FindLazy<TestTable6>();

            Assert.AreEqual(499, entities.Count, 0.000, "Failed to load TestTable1 Entity");

            int strValue = 1;

            foreach (var entity in entities)
            {
                Assert.AreEqual(strValue, entity.PrimaryKey, "Failed to fill TestTable1 Entity");
                Assert.AreEqual($"{strValue++}", entity.TestString, "Failed to fill TestTable1 Entity");
            }
        }

        [TestMethod]
        public void TestFindInheritanceLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = entityManager.Find<TestTable2>();

            int currValue = 5;
            int strValue = 2;
            int currPrimaryIdx = 0;
            int[] currPrimary = new int[] { 3, 2 };

            foreach (var entity in entities)
            {
                if (currValue == 5)
                    continue;

                Assert.AreEqual(currPrimary[currPrimaryIdx++], entity.TestPrimary2, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(currValue++, entity.Value, "Failed to fill TestTable2 Entity");
                Assert.AreEqual($"Test{strValue++}", entity.TestString, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(new DateTime(637548645720000000), entity.TestTimeStamp, "Failed to fill TestTable2 Entity");
                Assert.AreEqual(1, entity.TestDecimal, "Failed to fill TestTable2 Entity");
            }
        }

        [TestMethod]
        public void TestFindInheritanceAndPropertyWithEntityLazy()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entities = entityManager.Find<TestTable3>();

            int value = 1;

            foreach (var entity in entities)
            {
                if (entity.Table3 == null || entity.Table4 == null)
                    continue;

                Assert.AreEqual(4, entity.Table3.Count, "Failed to fill TestTable3 Entity");
                Assert.AreEqual(2, entity.Table4.Count, "Failed to fill TestTable3 Entity");
                Assert.AreEqual(value++, entity.PrimaryKey, "Failed to fill TestTable1 Entity");
            }
        }
    }
}
