﻿using System;
using CryEntity;

namespace UnitTest
{
    [CryEntityTableName("testtable6")]
    class TestTable6
    {
        [CryEntityColumnName("PrimaryKey")]
        [CryEntityPrimaryKey]
        [CryEntityAutoIncrement]
        public int PrimaryKey { get; set; }

        [CryEntityColumnName("CurrentTime")]
        public DateTime CurrentTime { get; set; }

        [CryEntityColumnName("TestString")]
        public string TestString { get; set; }


        public override string ToString()
        {
            return TestString;
        }
    }
}
