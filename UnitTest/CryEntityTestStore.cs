﻿using CryEntity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace UnitTest
{
    [TestClass]
    public class CryEntityTestStore
    {
        [TestMethod]
        public void TestStoreNormal()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = entityManager.Find<TestTable1>().Where((x) => x.Value > 7).First();

            var testValue = (decimal)new Random().NextDouble();

            entity.TestDecimal = testValue;

            entityManager.Store(entity);

            entity = entityManager.Find<TestTable1>().Where((x) => x.Value > 7).First();

            Assert.AreEqual(testValue, entity.TestDecimal, "Failed to store TestTable1 Entity");
        }

        [TestMethod]
        public void TestStoreInheritance()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = entityManager.Find<TestTable2>().Where((x) => x.Value > 7).First();

            var testValue = (decimal)new Random().NextDouble();

            entity.TestDecimal = testValue;

            entityManager.Store(entity);

            entity = entityManager.Find<TestTable2>().Where((x) => x.Value > 7).First();

            Assert.AreEqual(testValue, entity.TestDecimal, "Failed to store TestTable2 Entity");
        }

        [TestMethod]
        public void TestStoreInheritanceAndPropertyWithEntity()
        {
            CryEntityManager entityManager = new CryEntityManager(new TestDatabase());

            var entity = entityManager.Find<TestTable3>().Where((x) => x.PrimaryKey > 2).First();

            var testValue = (decimal)new Random().NextDouble();

            entity.Test.TestDecimal = testValue;

            entityManager.Store(entity);

            entity = entityManager.Find<TestTable3>().Where((x) => x.PrimaryKey > 2).First();

            Assert.AreEqual(testValue, entity.Test.TestDecimal, "Failed to store TestTable3 Entity");
        }
    }
}
