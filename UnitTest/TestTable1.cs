﻿using System;
using CryEntity;

namespace UnitTest
{
    [CryEntityTableName("testtable")]
    public class TestTable1
    {
        [CryEntityPrimaryKey]
        [CryEntityColumnName("TestPrimary")]
        [CryEntityAutoIncrement]
        public int Value { get; set; }

        [CryEntityColumnName("TestString")]
        public string TestString { get; set; }

        [CryEntityColumnName("TestDate")]
        public DateTime TestDate { get; set; }

        [CryEntityColumnName("TestDateTime")]
        public DateTime TestDateTime { get; set; }

        [CryEntityColumnName("TestBlob")]
        public byte[] TestBlob { get; set; }

        [CryEntityColumnName("TestDecimal")]
        public decimal TestDecimal { get; set; }

        [CryEntityColumnName("TestDouble")]
        public double TestDouble { get; set; }

        [CryEntityColumnName("TestFloat")]
        public float TestFloat { get; set; }

        [CryEntityColumnName("TestNull")]
        public string TestNull { get; set; }

        [CryEntityColumnName("TestTime")]
        public TimeSpan TestTime { get; set; }

        [CryEntityColumnName("TestTimeStamp")]
        public DateTime TestTimeStamp { get; set; }

        [CryEntityIgnore]
        public string Ignored { get; set; }
    }
}
