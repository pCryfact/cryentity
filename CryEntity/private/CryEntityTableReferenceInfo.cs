﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryEntity
{
    class CryEntityTableReferenceInfo
    {
        public CryEntityTableInfo TableInfo { get; }
        public CryEntityColumnInfo ColumnInfo { get; }
        public CryEntityForeignKeyInfo ForeignKeyInfo { get; }

        public CryEntityTableReferenceInfo(CryEntityTableInfo table, CryEntityColumnInfo columnInfo)
        {
            TableInfo = table;
            ColumnInfo = columnInfo;
            ForeignKeyInfo = null;
        }

        public CryEntityTableReferenceInfo(CryEntityTableInfo table, CryEntityColumnInfo columnInfo, CryEntityForeignKeyInfo foreignKeyInfo)
        {
            TableInfo = table;
            ColumnInfo = columnInfo;
            ForeignKeyInfo = foreignKeyInfo;
        }
    }
}
