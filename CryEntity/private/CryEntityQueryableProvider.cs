﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace CryEntity
{
    class CryEntityQueryableProvider : IQueryProvider
    {
        private delegate void FillAnonType<T>(DbDataReader reader, ICollection<T> result);

        private readonly static Dictionary<Type, Delegate> ms_typeMethodsCache;

        private static readonly ParameterExpression ms_readerParam = Expression.Parameter(typeof(DbDataReader), "reader");
        private static readonly MethodInfo ms_readerReadMethod = typeof(DbDataReader).GetMethod("Read");
        private static readonly ParameterExpression ms_objArrayVar = Expression.Variable(typeof(object[]), "param");
        private static readonly ParameterExpression ms_tmpVar = Expression.Variable(typeof(object), "tmp");
        private static readonly MethodInfo ms_fillTableMethod = typeof(CryEntityTableInfo).GetMethod("FillTable");
        private static readonly MethodInfo ms_fillMethod = typeof(CryEntityTableInfo).GetMethod("Fill");
        private static readonly MethodInfo ms_handleCollectionGroupingMethod = typeof(CryEntityQueryableProvider).GetMethod("HandleCollectionGrouping");
        private static readonly MethodInfo ms_fillAnonymousTypeMethod = typeof(CryEntityQueryableProvider).GetMethod("FillAnonymousType");
        private static readonly MethodInfo ms_handleCollectionSingleMethod = typeof(CryEntityQueryableProvider).GetMethod("HandleCollectionSingle");

        public CryEntityManager CryEntityManager { get; }

        public CryEntityTableInfo TableInfo { get; }

        public bool IsLazy { get; }

        private readonly DbConnection m_connection;

        static CryEntityQueryableProvider()
        {
            ms_typeMethodsCache = new Dictionary<Type, Delegate>();
        }

        public CryEntityQueryableProvider(CryEntityManager cryEntityManager, Type type, DbConnection connection, bool lazy)
        {
            CryEntityManager = cryEntityManager;
            m_connection = connection;
            IsLazy = lazy;

            TableInfo = CryEntityManager.CreateOrGetTableInfo(type);
        }

        public void Fill<T>(DbConnection dbConnection, Type resultType, CryEntityQueryStatement queryInfo, ICryEntityQueryBuilder queryBuilder, 
            Expression expression, ICollection<CryEntityCreateQueryColumnInfo> groupColumns, ICollection<T> result)
        {
            var cmd = dbConnection.CreateCommand();

            if (queryInfo.QueryInfo.Command != null)
                queryInfo.QueryInfo.Command.Dispose();

            queryInfo.QueryInfo.Command = cmd;

            string strQuery = queryBuilder.CreateSelectQuery(queryInfo);

            if (string.IsNullOrEmpty(strQuery))
                return;

            cmd.CommandText = strQuery;

            HandleCollection(resultType, queryInfo.IsUnionAll, queryInfo.QueryInfo.TableInfo.Type, groupColumns, expression, cmd, result);
        }

        public object Execute(Expression expression)
        {
            if (expression == null)
                throw new ArgumentNullException(nameof(expression));

            return Execute(expression.Type, expression);
        }

        public TResult Execute<TResult>(Expression expression)
        {
            if (expression == null)
                throw new ArgumentNullException(nameof(expression));

            return (TResult)Execute(typeof(TResult), expression);
        }

        public IQueryable CreateQuery(Expression expression)
        {
            if (expression == null)
                throw new ArgumentNullException(nameof(expression));

            if (!TryGetElementType(expression.Type, out Type elementType))
                throw new ArgumentException("Failed to get element type", nameof(expression));

            try
            {
                return CreateQuery(expression, elementType);
            }
            catch (TargetInvocationException ex)
            {
                throw new ArgumentException(null, nameof(expression), ex.InnerException);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(null, nameof(expression), ex);
            }
        }

        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            return (IQueryable<TElement>)CreateQuery(expression, typeof(TElement));
        }

        // called via reflection
        public void FillAnonymousType<T>(Expression expression, Type realElementType, DbDataReader reader, bool isUnionAll, 
            ICollection<CryEntityCreateQueryColumnInfo> groupColumns, ICollection<T> result)
        {
            Type elementType = typeof(T);

            if (!ms_typeMethodsCache.TryGetValue(elementType, out var method))
            {
                var properties = GetTableProperties(elementType);
                var anonColumns = TableInfo.GetColumnsWithAnonType(elementType);
                var keyProperty = elementType.GetProperty("Key");

                if (groupColumns != null)
                    method = HandleAnonTypeAndGroupColumns(realElementType, elementType, reader, keyProperty, groupColumns, expression, result);
                else if (properties.Count > 0)
                    method = HandleAnonTypeAndProperties(realElementType, elementType, isUnionAll, reader, keyProperty, properties, expression, result);
                else
                    method = HandleAnonTypeAndAnonColumns(realElementType, elementType, reader, keyProperty, anonColumns, expression, result);
            }

            method.DynamicInvoke(reader, result);
        }

        // called via reflection
        public void HandleCollectionGrouping<T>(Expression expression, bool isUnionAll, 
            ICollection<CryEntityCreateQueryColumnInfo> groupColumns, Type[] genericArgs, DbDataReader reader, ICollection<T> result)
        {
            Type elementType = typeof(T);

            if (!TryGetElementType(elementType, out Type realElementType))
                throw new ArgumentException("Failed to get element type", nameof(T));

            if (realElementType == TableInfo.Type)
            {
                var keyProperty = elementType.GetProperty("Key");

                if (IsAnonymousType(genericArgs[0]))
                {
                    var method = ms_fillAnonymousTypeMethod.MakeGenericMethod(elementType);
                    method.Invoke(this, new object[] { expression, genericArgs[0], reader, isUnionAll, groupColumns, result });
                }
                else
                {
                    while (reader.Read())
                    {
                        var obj = (T)Activator.CreateInstance(elementType, new object[] { this, expression });

                        foreach (var column in groupColumns)
                        {
                            var tmp = reader[column.Alias];

                            keyProperty.SetValue(obj, tmp);
                        }

                        result.Add(obj);
                    }
                }
            }
        }

        // called via reflection
        public static void HandleCollectionSingle<T>(DbDataReader reader, ICollection<T> result)
        {
            while (reader.Read())
                result.Add((T)reader.GetValue(0));
        }

        internal static bool TryGetElementType(Type sequenceType, out Type elementType)
        {
            if (sequenceType == null || sequenceType.IsPrimitive)
            {
                elementType = null;
                return false;
            }

            return TryGetArrayElementType(sequenceType, out elementType) ||
                TryGetGenericElementType(sequenceType, out elementType) ||
                TryGetInterfaceElementType(sequenceType, out elementType) ||
                TryGetElementType(sequenceType.BaseType, out elementType);
        }

        internal static bool IsAnonymousType(Type type)
        {
            return type.GetCustomAttributes(typeof(CompilerGeneratedAttribute), false).Length > 0 &&
                type.FullName.Contains("AnonymousType") &&
                type.Namespace == null &&
                type.Name.StartsWith("<>");
        }

        private IQueryable CreateQuery(Expression expression, Type elementType)
        {
            var genericArgs = elementType.GetGenericArguments();

            if (genericArgs.Length == 2 && elementType == typeof(IGrouping<,>).MakeGenericType(genericArgs))
                elementType = typeof(CryEntityGroupedQueryableCollection<,>).MakeGenericType(genericArgs);

            if (expression.Type == typeof(IOrderedQueryable<>).MakeGenericType(elementType))
            {
                return (IQueryable)Activator.CreateInstance(
                    typeof(CryEntityOrderedQueryableCollection<>).MakeGenericType(elementType), new object[] { this, expression });
            }

            return (IQueryable)Activator.CreateInstance(
                typeof(CryEntityQueryableCollection<>).MakeGenericType(elementType), new object[] { this, expression });
        }

        private void FillMissingQueryData(CryEntityExpressionVisitor visitor, DbCommand cmd, out bool onlyCount, out int countAggrCount)
        {
            var curStatement = visitor.QueryStatement;
            countAggrCount = 0;
            onlyCount = true;

            while (curStatement != null)
            {
                curStatement.QueryInfo.Command = cmd;

                if (curStatement.QueryInfo.Columns.Count == 0)
                {
                    if (curStatement.QueryInfo.Joins.Count == 0)
                    {
                        curStatement.QueryInfo.Columns = CryEntityManager.GetAllColumns(TableInfo);
                        curStatement.QueryInfo.Joins = GetJoinsFromColumns(curStatement.QueryInfo.TableInfo, curStatement.QueryInfo.Columns);
                    }
                    else
                    {
                        var lst = curStatement.QueryInfo.Joins.Select((x) => x.TableInfo).ToList();
                        lst.Insert(0, TableInfo);
                        curStatement.QueryInfo.Columns = CryEntityManager.GetAllColumns(lst.ToArray());
                    }
                }
                else if (curStatement.QueryInfo.Joins.Count == 0)
                    curStatement.QueryInfo.Joins = CryEntityManager.GetAllJoins(TableInfo);

                if (curStatement.QueryInfo.Manipulator != CryEntityColumnManipulator.Count)
                {
                    bool countFound = false;

                    foreach (var column in curStatement.QueryInfo.Columns)
                    {
                        if (column.Manipulator == CryEntityColumnManipulator.Count)
                        {
                            ++countAggrCount;
                            countFound = true;
                        }
                    }

                    if (!countFound)
                        onlyCount = false;
                }
                else
                    ++countAggrCount;

                if (curStatement.QueryInfo.Limit < 0 || curStatement.QueryInfo.Offset < 0)
                {
                    var oldManipulator = curStatement.QueryInfo.Manipulator;
                    curStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Count;

                    var oldLimit = curStatement.QueryInfo.Limit;
                    var oldOffset = curStatement.QueryInfo.Offset;

                    curStatement.QueryInfo.Limit = 0;
                    curStatement.QueryInfo.Offset = 0;

                    string strQuery = CryEntityManager.QueryBuilder.CreateSelectQuery(curStatement);
                    curStatement.QueryInfo.Manipulator = oldManipulator;

                    curStatement.QueryInfo.Limit = oldLimit;
                    curStatement.QueryInfo.Offset = oldOffset;

                    if (string.IsNullOrEmpty(strQuery))
                        throw new InvalidOperationException();

                    cmd.CommandText = strQuery;

                    long count = (long)cmd.ExecuteScalar();

                    if (curStatement.QueryInfo.Limit < 0 && curStatement.QueryInfo.Offset < 0)
                    {
                        curStatement.QueryInfo.Limit = count + curStatement.QueryInfo.Limit;
                        curStatement.QueryInfo.Offset = 0;
                    }
                    else
                    {
                        curStatement.QueryInfo.Offset = count - curStatement.QueryInfo.Limit;
                        curStatement.QueryInfo.Limit = 0;
                    }

                    cmd.Dispose();
                    cmd = m_connection.CreateCommand();
                }

                curStatement = curStatement.Next;
            }

        }

        private FillAnonType<T> CompileAnonTypeWithAnonColumnsMethod<T>(Type realElementType, Expression expression, 
            Dictionary<string, Tuple<Tuple<int, PropertyInfo>, CryEntityColumnInfo>> anonColumns)
        {
            Type elementType = typeof(T);
            var exitLoopLabel = Expression.Label("loopExit");
            var collectionParam = Expression.Parameter(typeof(ICollection<T>), "result");
            var collectionAddMethod = collectionParam.Type.GetMethod("Add");

            var fillObjectExpressions = new List<Expression>();
            var objectFillParams = new List<Expression>();

            int idx = 0;

            var variables = new List<ParameterExpression>();

            ParameterExpression objVar = Expression.Variable(elementType);

            variables.Add(ms_tmpVar);

            if (expression != null && realElementType != null)
                variables.Add(objVar);

            foreach (var prop in anonColumns)
            {
                fillObjectExpressions.Add(Expression.Assign(ms_tmpVar, Expression.Property(ms_readerParam, "Item", Expression.Constant(prop.Key))));

                fillObjectExpressions.Add(Expression.IfThenElse(Expression.Equal(ms_tmpVar, Expression.Constant(DBNull.Value)),
                     Expression.Assign(Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(prop.Value.Item1.Item1)), 
                        Expression.Convert(Expression.Default(prop.Value.Item1.Item2.PropertyType), typeof(object))),
                     Expression.Assign(Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(prop.Value.Item1.Item1)), ms_tmpVar)));

                objectFillParams.Add(Expression.Convert(Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(idx)), prop.Value.Item1.Item2.PropertyType));
                ++idx;
            }

            if (expression != null && realElementType != null)
            {
                var ctorInfo = elementType.GetConstructors()[0];
                var realCtorInfo = realElementType.GetConstructors()[0];

                fillObjectExpressions.Add(Expression.Assign(objVar, Expression.New(ctorInfo, Expression.Constant(this), Expression.Constant(expression))));
                fillObjectExpressions.Add(Expression.Assign(Expression.Property(objVar, "Key"), Expression.New(realCtorInfo, objectFillParams)));
                fillObjectExpressions.Add(Expression.Call(collectionParam, collectionAddMethod, objVar));
            }
            else
            {
                var ctorInfo = elementType.GetConstructors()[0];

                fillObjectExpressions.Add(Expression.Call(collectionParam, collectionAddMethod, Expression.New(ctorInfo, objectFillParams)));
            }

            var loop = Expression.Loop(
                Expression.IfThenElse(
                    Expression.Call(ms_readerParam, ms_readerReadMethod), 
                    Expression.Block(variables, fillObjectExpressions), 
                    Expression.Break(exitLoopLabel)), 
                exitLoopLabel);

            var loopBody = Expression.Block(new[] { ms_objArrayVar }, 
                Expression.Assign(ms_objArrayVar, Expression.NewArrayBounds(typeof(object), 
                    Expression.Constant(anonColumns.Count, typeof(int)))), 
                loop);
            
            
            var lambda = Expression.Lambda<FillAnonType<T>>(loopBody, ms_readerParam, collectionParam);
            return lambda.Compile();
        }

        private FillAnonType<T> CompileAnonTypeWithGroupColumnsMethod<T>(Type realElementType, 
            Expression expression, ICollection<CryEntityCreateQueryColumnInfo> groupColumns)
        {
            Type elementType = typeof(T);
            var exitLoopLabel = Expression.Label("loopExit");
            var collectionParam = Expression.Parameter(typeof(ICollection<T>), "result");
            var collectionAddMethod = collectionParam.Type.GetMethod("Add");

            var fillObjectExpressions = new List<Expression>();
            var objectFillParams = new List<Expression>();

            ParameterExpression objVar = Expression.Variable(elementType);

            var variables = new List<ParameterExpression>
            {
                ms_tmpVar
            };

            if (expression != null && realElementType != null)
                variables.Add(objVar);

            foreach (var prop in groupColumns)
            {
                fillObjectExpressions.Add(Expression.Assign(ms_tmpVar, Expression.Property(ms_readerParam, "Item", Expression.Constant(prop.Alias))));

                fillObjectExpressions.Add(Expression.IfThenElse(Expression.Equal(ms_tmpVar, Expression.Constant(DBNull.Value)),
                     Expression.Assign(Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(prop.Index)), 
                        Expression.Convert(Expression.Default(prop.PropertyType), typeof(object))),
                     Expression.Assign(Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(prop.Index)), ms_tmpVar)));

                objectFillParams.Add(Expression.Convert(Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(prop.Index)), prop.PropertyType));
            }

            if (expression != null && realElementType != null)
            {
                var ctorInfo = elementType.GetConstructors()[0];
                var realCtorInfo = realElementType.GetConstructors()[0];

                fillObjectExpressions.Add(Expression.Assign(objVar, Expression.New(ctorInfo, Expression.Constant(this), Expression.Constant(expression))));
                fillObjectExpressions.Add(Expression.Assign(Expression.Property(objVar, "Key"), Expression.New(realCtorInfo, objectFillParams)));
                fillObjectExpressions.Add(Expression.Call(collectionParam, collectionAddMethod, objVar));
            }
            else
            {
                var ctorInfo = elementType.GetConstructors()[0];

                fillObjectExpressions.Add(Expression.Call(collectionParam, collectionAddMethod, Expression.New(ctorInfo, objectFillParams)));
            }

            var loop = Expression.Loop(
                Expression.IfThenElse(
                    Expression.Call(ms_readerParam, ms_readerReadMethod), 
                    Expression.Block(variables, fillObjectExpressions), 
                    Expression.Break(exitLoopLabel)), 
                exitLoopLabel);

            var loopBody = Expression.Block(new[] { ms_objArrayVar }, 
                Expression.Assign(ms_objArrayVar, 
                    Expression.NewArrayBounds(typeof(object), 
                    Expression.Constant(groupColumns.Count, typeof(int)))), 
                loop);


            var lambda = Expression.Lambda<FillAnonType<T>>(loopBody, ms_readerParam, collectionParam);
            return lambda.Compile();
        }

        private FillAnonType<T> CompileAnonTypeWithTableMethod<T>(Type realElementType, Expression expression, 
            List<Tuple<PropertyInfo, CryEntityTableInfo>> properties, bool isUnionAll)
        {
            Type elementType = typeof(T);
            var exitLoopLabel = Expression.Label("loopExit");
            var collectionParam = Expression.Parameter(typeof(ICollection<T>), "result");
            var collectionAddMethod = collectionParam.Type.GetMethod("Add");

            var fillObjectExpressions = new List<Expression>();
            var objectFillParams = new List<Expression>();
            int idx = 0;

            var variables = new List<ParameterExpression>();
            ParameterExpression objVar = Expression.Variable(elementType);

            if (expression != null && realElementType != null)
                variables.Add(objVar);

            foreach (var prop in properties)
            {
                var methodInfo = ms_fillTableMethod.MakeGenericMethod(prop.Item1.PropertyType);
                fillObjectExpressions.Add(Expression.Assign(
                    Expression.ArrayAccess(ms_objArrayVar, Expression.Constant(idx)),
                    Expression.Call(
                        Expression.Constant(prop.Item2), methodInfo, ms_readerParam, Expression.Constant(isUnionAll))));

                objectFillParams.Add(Expression.Convert(Expression.ArrayIndex(ms_objArrayVar, Expression.Constant(idx)), prop.Item1.PropertyType));
                ++idx;
            }

            if (expression != null && realElementType != null)
            {
                var ctorInfo = elementType.GetConstructors()[0];
                var realCtorInfo = realElementType.GetConstructors()[0];

                fillObjectExpressions.Add(Expression.Assign(objVar, Expression.New(ctorInfo, Expression.Constant(this), Expression.Constant(expression))));
                fillObjectExpressions.Add(Expression.Assign(Expression.Property(objVar, "Key"), Expression.New(realCtorInfo, objectFillParams)));
                fillObjectExpressions.Add(Expression.Call(collectionParam, collectionAddMethod, objVar));
            }
            else
            {
                var ctorInfo = elementType.GetConstructors()[0];

                fillObjectExpressions.Add(Expression.Call(collectionParam, collectionAddMethod, Expression.New(ctorInfo, objectFillParams)));
            }

            var loop = Expression.Loop(
                Expression.IfThenElse(
                    Expression.Call(ms_readerParam, ms_readerReadMethod), 
                    Expression.Block(variables, fillObjectExpressions), 
                    Expression.Break(exitLoopLabel)), 
                exitLoopLabel);

            var loopBody = Expression.Block(new[] { ms_objArrayVar }, 
                Expression.Assign(ms_objArrayVar, Expression.NewArrayBounds(typeof(object), 
                    Expression.Constant(properties.Count, typeof(int)))), 
                loop);

            var lambda = Expression.Lambda<FillAnonType<T>>(loopBody, ms_readerParam, collectionParam);
            return lambda.Compile();
        }

        private object HandleNonCollection(Type resultType, bool isUnionAll, Type tableType, DbCommand cmd)
        {
            if (resultType == tableType)
            {
                using var reader = cmd.ExecuteReader();

                var listType = CryEntityManager.PreferredCollection.MakeGenericType(resultType);
                var result = (IEnumerable)Activator.CreateInstance(listType);

                var methodInfo = ms_fillMethod.MakeGenericMethod(resultType);
                methodInfo.Invoke(TableInfo, new object[] { reader, result, isUnionAll });

                cmd.Dispose();

                return result.Cast<object>().First();
            }
            else if (IsAnonymousType(resultType))
            {
                var listType = CryEntityManager.PreferredList.MakeGenericType(resultType);
                var result = (ICollection)Activator.CreateInstance(listType);
                using var reader = cmd.ExecuteReader();

                var method = ms_fillAnonymousTypeMethod.MakeGenericMethod(resultType);
                method.Invoke(this, new object[] { null, null, reader, isUnionAll, null, result });

                cmd.Dispose();

                return result.Cast<object>().First();
            }
            else
            {
                var result = cmd.ExecuteScalar();
                cmd.Dispose();
                return result;
            }
        }

        private object HandleCollection(Type resultType, 
            ICollection<CryEntityCreateQueryColumnInfo> groupColumns, Expression expression, DbCommand cmd, CryEntityQueryStatement queryStatement)
        {
            if (!TryGetElementType(resultType, out Type elementType))
                throw new ArgumentException("Failed to get element type", nameof(resultType));

            object result;

            if (IsLazy)
            {
                var newConnection = CryEntityManager.DataBase.CreateConnection();

                newConnection.Open();

                Type listType = typeof(CryEntityQueryLazyCollection<>).MakeGenericType(elementType);
                result = Activator.CreateInstance(listType, new object[] { this, expression, newConnection, CryEntityManager.QueryBuilder,
                    queryStatement, 0, CryEntityManager.PreferredList, groupColumns, resultType });
            }
            else
            {
                Type listType = CryEntityManager.PreferredList.MakeGenericType(elementType);
                result = Activator.CreateInstance(listType);

                HandleCollection(resultType, queryStatement.IsUnionAll, queryStatement.QueryInfo.TableInfo.Type, groupColumns, expression, cmd, result);
            }

            return result;
        }

        private void HandleCollection(Type resultType, bool isUnionAll, Type tableType,
            ICollection<CryEntityCreateQueryColumnInfo> groupColumns, Expression expression, DbCommand cmd, object result)
        {
            if (!TryGetElementType(resultType, out Type elementType))
                throw new ArgumentException("Failed to get element type", nameof(resultType));

            using var reader = cmd.ExecuteReader();
            var genericArgs = elementType.GetGenericArguments();

            if (genericArgs.Length == 2 && typeof(IGrouping<,>).MakeGenericType(genericArgs).IsAssignableFrom(elementType))
            {
                var method = ms_handleCollectionGroupingMethod.MakeGenericMethod(elementType);
                method.Invoke(this, new object[] { expression, isUnionAll, groupColumns, genericArgs, reader, result });
            }
            else if (IsAnonymousType(elementType))
            {
                var method = ms_fillAnonymousTypeMethod.MakeGenericMethod(elementType);
                method.Invoke(this, new object[] { null, null, reader, isUnionAll, groupColumns, result });
            }
            else if (elementType == tableType)
            {
                var methodInfo = ms_fillMethod.MakeGenericMethod(elementType);
                methodInfo.Invoke(TableInfo, new object[] { reader, result, isUnionAll });
            }
            else
            {
                var method = ms_handleCollectionSingleMethod.MakeGenericMethod(elementType);
                method.Invoke(this, new object[] { reader, result });
            }
        }

        private object Execute(Type resultType, Expression expression)
        {
            var visitor = new CryEntityExpressionVisitor();
            visitor.Visit(expression);

            var cmd = m_connection.CreateCommand();

            FillMissingQueryData(visitor, cmd, out var onlyCount, out var countAggrCount);
            OptimizeQueryData(visitor);

            if (onlyCount && countAggrCount == 1)
            {
                string strQuery = CryEntityManager.QueryBuilder.CreateSelectQuery(visitor.QueryStatement);

                if (!string.IsNullOrEmpty(strQuery))
                {
                    cmd.CommandText = strQuery;

                    var result = Convert.ChangeType(cmd.ExecuteScalar(), resultType);
                    cmd.Dispose();
                    return result;
                }
            }
            else
            {
                GetColumnAndGroupLists(visitor.QueryStatement, out var columnInfos, out var groupColumns);

                string strQuery = CryEntityManager.QueryBuilder.CreateSelectQuery(visitor.QueryStatement);

                if (columnInfos != null)
                    visitor.QueryStatement.QueryInfo.Columns = columnInfos;

                if (!string.IsNullOrEmpty(strQuery))
                {
                    cmd.CommandText = strQuery;

                    if (typeof(IEnumerable).IsAssignableFrom(resultType))
                        return HandleCollection(resultType, groupColumns, expression, cmd, visitor.QueryStatement);
                    else
                        return HandleNonCollection(resultType, visitor.QueryStatement.IsUnionAll, visitor.QueryStatement.QueryInfo.TableInfo.Type, cmd);
                }
            }

            cmd.Dispose();
            return null;
        }

        private Delegate HandleAnonTypeAndGroupColumns<T>(Type realElementType, Type elementType, DbDataReader reader, PropertyInfo keyProperty,
            ICollection<CryEntityCreateQueryColumnInfo> groupColumns, Expression expression, ICollection<T> result)
        {
            bool isCompiled = false;
            Delegate compiledMethod = null;

            Task compileTask = Task.Run(() =>
            {
                compiledMethod = CompileAnonTypeWithGroupColumnsMethod<T>(realElementType, expression, groupColumns);
                ms_typeMethodsCache.Add(elementType, compiledMethod);
                isCompiled = true;
            });

            object[] param = new object[groupColumns.Count];

            while (reader.Read())
            {
                foreach (var column in groupColumns)
                {
                    var tmp = reader[column.Alias];

                    if (tmp == DBNull.Value)
                        param[column.Index] = null;
                    else
                        param[column.Index] = Convert.ChangeType(tmp, column.PropertyType);
                }

                if (expression != null && realElementType != null)
                {
                    var obj = (T)Activator.CreateInstance(elementType, new object[] { this, expression });
                    var objTmp = Activator.CreateInstance(realElementType, param);

                    keyProperty.SetValue(obj, objTmp);

                    result.Add(obj);
                }
                else
                {
                    var obj = (T)Activator.CreateInstance(elementType, param);
                    result.Add(obj);
                }

                if (isCompiled)
                    break;
            }

            compileTask.Wait();
            return compiledMethod;
        }

        private Delegate HandleAnonTypeAndProperties<T>(Type realElementType, Type elementType, bool isUnionAll, DbDataReader reader, PropertyInfo keyProperty, 
            List<Tuple<PropertyInfo, CryEntityTableInfo>> properties, Expression expression, ICollection<T> result)
        {
            bool isCompiled = false;
            Delegate compiledMethod = null;

            Task compileTask = Task.Run(() =>
            {
                compiledMethod = CompileAnonTypeWithTableMethod<T>(realElementType, expression, properties, isUnionAll);
                ms_typeMethodsCache.Add(elementType, compiledMethod);
                isCompiled = true;
            });

            var fillTableMethod = typeof(CryEntityTableInfo).GetMethod("FillTable");
            object[] param = new object[properties.Count];
            int idx = 0;

            while (reader.Read())
            {
                idx = 0;

                foreach (var prop in properties)
                {
                    var methodInfo = fillTableMethod.MakeGenericMethod(prop.Item1.PropertyType);
                    param[idx++] = methodInfo.Invoke(prop.Item2, new object[] { reader, isUnionAll });
                }

                if (expression != null && realElementType != null)
                {
                    var obj = (T)Activator.CreateInstance(elementType, new object[] { this, expression });
                    var objTmp = Activator.CreateInstance(realElementType, param);

                    keyProperty.SetValue(obj, objTmp);

                    result.Add(obj);
                }
                else
                {
                    var obj = (T)Activator.CreateInstance(elementType, param);
                    result.Add(obj);
                }

                if (isCompiled)
                    break;
            }

            compileTask.Wait();
            return compiledMethod;
        }

        private Delegate HandleAnonTypeAndAnonColumns<T>(Type realElementType, Type elementType, DbDataReader reader, PropertyInfo keyProperty,
            Dictionary<string, Tuple<Tuple<int, PropertyInfo>, CryEntityColumnInfo>> anonColumns, Expression expression, ICollection<T> result)
        {
            bool isCompiled = false;
            Delegate compiledMethod = null;

            Task compileTask = Task.Run(() =>
            {
                compiledMethod = CompileAnonTypeWithAnonColumnsMethod<T>(realElementType, expression, anonColumns);
                ms_typeMethodsCache.Add(elementType, compiledMethod);
                isCompiled = true;
            });

            object[] param = new object[anonColumns.Count];

            while (reader.Read())
            {
                foreach (var prop in anonColumns)
                {
                    var tmp = reader[prop.Key];

                    if (tmp == DBNull.Value)
                        param[prop.Value.Item1.Item1] = null;
                    else
                        param[prop.Value.Item1.Item1] = Convert.ChangeType(tmp, prop.Value.Item1.Item2.PropertyType);
                }

                if (expression != null && realElementType != null)
                {
                    var obj = (T)Activator.CreateInstance(elementType, new object[] { this, expression });
                    var objTmp = Activator.CreateInstance(realElementType, param);

                    keyProperty.SetValue(obj, objTmp);

                    result.Add(obj);
                }
                else
                {
                    var obj = (T)Activator.CreateInstance(elementType, param);
                    result.Add(obj);
                }

                if (isCompiled)
                    break;
            }

            compileTask.Wait();
            return compiledMethod;
        }

        private static bool TryGetArrayElementType(Type sequenceType, out Type elementType)
        {
            return (elementType = sequenceType.IsArray ? sequenceType.GetElementType() : null) != null;
        }

        private static void GetColumnAndGroupLists(CryEntityQueryStatement queryStatement, 
            out ICollection<CryEntityCreateQueryColumnInfo> columnInfos, out ICollection<CryEntityCreateQueryColumnInfo> groupColumns)
        {
            columnInfos = null;
            groupColumns = null;

            if (queryStatement.QueryInfo.GroupBy != null)
            {
                columnInfos = queryStatement.QueryInfo.Columns;

                var newColumnList = new List<CryEntityCreateQueryColumnInfo>();

                var curGroup = queryStatement.QueryInfo.GroupBy.GroupInfo;

                while (curGroup != null)
                {
                    newColumnList.Add(curGroup.ColumnInfo);

                    curGroup = curGroup.Next;
                }

                foreach (var column in queryStatement.QueryInfo.GroupBy.AdditionalColumns)
                    newColumnList.Add(column);


                groupColumns = newColumnList;
                queryStatement.QueryInfo.Columns = newColumnList;
            }
        }

        private static List<CryEntityQueryJoinInfo> GetJoinsFromColumns(CryEntityTableInfo tableInfo,
            ICollection<CryEntityCreateQueryColumnInfo> columnInfos)
        {
            var result = new List<CryEntityQueryJoinInfo>();

            foreach (var a in columnInfos)
            {
                if (a.ColumnInfo.TableInfo != tableInfo)
                    result.Add(GetJoinFromTableInfo(tableInfo, a.ColumnInfo.TableInfo));
            }

            return result;
        }

        private static bool TryGetGenericElementType(Type sequenceType, out Type elementType)
        {
            elementType = null;

            if (!sequenceType.IsGenericType)
                return false;

            Type[] argumentTypes = sequenceType.GetGenericArguments();

            if (argumentTypes.Length < 1)
                return false;

            Type firstArgumentType = argumentTypes[0];

            Type enumerableType = typeof(IEnumerable<>).MakeGenericType(firstArgumentType);

            if (!enumerableType.IsAssignableFrom(sequenceType))
                return false;

            elementType = firstArgumentType;
            return true;
        }

        private static bool TryGetInterfaceElementType(Type sequenceType, out Type elementType)
        {
            foreach (Type interfaceType in sequenceType.GetInterfaces())
            {
                if (TryGetElementType(interfaceType, out elementType))
                    return true;
            }

            elementType = null;
            return false;
        }

        private static CryEntityQueryJoinInfo GetJoinFromTableInfo(CryEntityTableInfo table1, CryEntityTableInfo table2)
        {
            return null; // TODO: add this
        }

        private static void OptimizeQueryData(CryEntityExpressionVisitor visitor)
        {
            var curStatement = visitor.QueryStatement;

            while (curStatement != null)
            {
                // optimize manipulator
                if (curStatement.QueryInfo.Manipulator != CryEntityColumnManipulator.None && curStatement.QueryInfo.Columns.Count == 1)
                {
                    curStatement.QueryInfo.Columns.First().Manipulator = curStatement.QueryInfo.Manipulator;
                    curStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.None;
                }

                curStatement = curStatement.Next;
            }
        }

        private static List<Tuple<PropertyInfo, CryEntityTableInfo>> GetTableProperties(Type type)
        {
            var result = new List<Tuple<PropertyInfo, CryEntityTableInfo>>();
            var tmp = type.GetProperties();

            foreach (var prop in tmp)
            {
                if (CryEntityManager.TableInfo.TryGetValue(prop.PropertyType, out var tableInfo))
                    result.Add(new Tuple<PropertyInfo, CryEntityTableInfo>(prop, tableInfo));
            }

            return result;
        }
    }
}
