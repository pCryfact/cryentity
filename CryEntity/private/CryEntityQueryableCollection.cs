﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CryEntity
{
    class CryEntityQueryableCollection<T> : IQueryable<T>
    {
        public Type ElementType => typeof(T);

        public Expression Expression { get; }

        public IQueryProvider Provider => m_provider;

        private readonly CryEntityQueryableProvider m_provider;

        public CryEntityQueryableCollection(CryEntityQueryableProvider provider, Expression expression)
        {
            m_provider = provider;
            Expression = expression;
        }

        public CryEntityQueryableCollection(CryEntityQueryableProvider provider)
        {
            m_provider = provider;
            Expression = Expression.Constant(this);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Provider.Execute<IEnumerable<T>>(Expression).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
