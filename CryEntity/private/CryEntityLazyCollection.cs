﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace CryEntity
{
    class CryEntityLazyCollection<T> : CryEntityLazyCollectionBase<T>
    {
        private readonly CryEntityTableInfo m_tableInfo;

        public CryEntityLazyCollection(CryEntityTableInfo tableInfo, DbConnection connection, ICryEntityQueryBuilder queryBuilder,
            CryEntityQueryStatement queryCreateInfo, int realLimit, Type prefferedList)
            : base(connection, queryBuilder, queryCreateInfo, realLimit, prefferedList)
        {
            m_tableInfo = tableInfo;
            Pull();
        }

        public override void Pull()
        {
            using var cmdSelect = DbConnection.CreateCommand();

            if (QueryCreateInfo.QueryInfo.Command != null)
                QueryCreateInfo.QueryInfo.Command.Dispose();

            QueryCreateInfo.QueryInfo.Command = cmdSelect;

            cmdSelect.CommandText = QueryBuilder.CreateSelectQuery(QueryCreateInfo);

            using var reader = cmdSelect.ExecuteReader();

            m_tableInfo.Fill(reader, InternalList, QueryCreateInfo.IsUnionAll);

            QueryCreateInfo.QueryInfo.Offset += QueryCreateInfo.QueryInfo.Limit;
        }
    }
}
