﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq.Expressions;

namespace CryEntity
{
    class CryEntityQueryLazyCollection<T> : CryEntityLazyCollectionBase<T>
    {
        private readonly CryEntityQueryableProvider m_provider;
        private readonly Expression m_nextExpression;
        private readonly ICollection<CryEntityCreateQueryColumnInfo> m_groupColumns;
        private readonly Type m_resultType;

        // called via reflection
        public CryEntityQueryLazyCollection(CryEntityQueryableProvider provider, Expression nextExpr, DbConnection connection, ICryEntityQueryBuilder queryBuilder,
            CryEntityQueryStatement queryCreateInfo, int realLimit, Type prefferedList, ICollection<CryEntityCreateQueryColumnInfo> groupColumns, Type resultType)
            : base(connection, queryBuilder, queryCreateInfo, realLimit, prefferedList)
        {
            m_provider = provider;
            m_nextExpression = nextExpr;
            m_groupColumns = groupColumns;
            m_resultType = resultType;

            Pull();
        }

        public override void Pull()
        {
            m_provider.Fill(DbConnection, m_resultType, QueryCreateInfo, QueryBuilder, m_nextExpression, m_groupColumns, InternalList);

            QueryCreateInfo.QueryInfo.Offset += QueryCreateInfo.QueryInfo.Limit;
        }
    }
}
