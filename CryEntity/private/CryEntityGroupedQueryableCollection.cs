﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace CryEntity
{
    class CryEntityGroupedQueryableCollection<TKey, T> : IGrouping<TKey, T>
    {
        public static Type ElementType => typeof(T);

        public Expression Expression { get; }

        public IQueryProvider Provider => m_provider;

        public TKey Key { get; set; }

        private readonly CryEntityQueryableProvider m_provider;

        public CryEntityGroupedQueryableCollection(CryEntityQueryableProvider provider, Expression expression)
        {
            m_provider = provider;
            Expression = expression;
        }

        public IEnumerator<T> GetEnumerator()
        {
            var replacer = new CryEntityGroupByExpressionReplacer(Key);
            var expr = replacer.Visit(Expression);

            return Provider.Execute<IEnumerable<T>>(expr).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
