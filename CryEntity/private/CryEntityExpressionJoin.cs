﻿
namespace CryEntity
{
    class CryEntityExpressionJoin
    {
        public CryEntityTableInfo TableInfo { get; set; }
        public CryEntityJoinType JoinType { get; set; }
        public CryEntityQueryWhereOnInfo OnInfo { get; set; }
    }
}
