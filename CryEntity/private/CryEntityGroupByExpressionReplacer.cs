﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CryEntity
{
    class CryEntityGroupByExpressionReplacer : ExpressionVisitor
    {
        public object Key { get; }

        private Type m_delegateType;
        private bool m_bvisitLambda;
        private bool m_bvisitMember;

        public CryEntityGroupByExpressionReplacer(object key)
        {
            Key = key;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.Name == "GroupBy")
            {
                m_bvisitLambda = true;
                var param = Visit(node.Arguments[1]);
                m_bvisitLambda = false;

                var method = node.Method.DeclaringType.GetMethods()
                         .Where(m => m.Name == "Where")
                         .Select(m => new {
                             Method = m,
                             Params = m.GetParameters(),
                             Args = m.GetGenericArguments()
                         })
                         .Where(x => x.Params.Length == 2
                                     && x.Args.Length == 1
                                     && x.Params[0].ParameterType == typeof(IQueryable<>).MakeGenericType(x.Args[0]))
                         .Select(x => x.Method)
                         .First();

                return Expression.Call(method.MakeGenericMethod(node.Arguments[0].Type.GetGenericArguments()[0]), node.Arguments[0], param);
            }
            else if (node.Method.Name.Contains("First") || node.Method.Name.Contains("Last") || 
                (node.Method.Name == "Take") || (node.Method.Name == "Skip") || 
                (node.Method.Name == "TakeLast") || (node.Method.Name == "SkipLast") || node.Method.Name == "Where")
            {
                return Visit(node.Arguments[0]); // ignore
            }
            else
            {
                var newExpr = Visit(node.Arguments[0]);
                var param = Visit(node.Arguments[1]) as UnaryExpression;
                var returnType = (param.Operand as LambdaExpression).ReturnType;

                var method = node.Method.GetGenericMethodDefinition().MakeGenericMethod(returnType, returnType);

                return Expression.Call(method, newExpr, param);
            }
        }

        protected override Expression VisitLambda<T>(Expression<T> node)
        {
            if (m_bvisitLambda)
            {
                var inType = typeof(T).GetGenericArguments()[0];
                m_delegateType = typeof(Func<,>).MakeGenericType(inType, typeof(bool));

                return Expression.Lambda(m_delegateType, Expression.Equal(node.Body, Expression.Constant(Key)), node.Parameters);
            }
            else if (typeof(IGrouping<,>).IsAssignableFrom(node.ReturnType.GetGenericTypeDefinition()))
            {
                m_bvisitMember = true;
                var parameter = Visit(node.Body);
                m_bvisitMember = false;
                var type = typeof(Func<,>).MakeGenericType(node.ReturnType.GetGenericArguments()[1], node.ReturnType.GetGenericArguments()[1]);
                
                return Expression.Lambda(type, parameter, (ParameterExpression)parameter);
            }

            return base.VisitLambda(node);
        }
        protected override Expression VisitParameter(ParameterExpression node)
        {
            if (m_bvisitMember)
                return base.VisitParameter(Expression.Parameter(node.Type.GetGenericArguments()[1], node.Name));

            return base.VisitParameter(node);
        }
    }
}
