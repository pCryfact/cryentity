using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace CryEntity
{
    enum CryEntityQueryMethod
    {
        None,
        Select,
        JoinSource,
        JoinDest,
        Where,
        WhereFirst,
        WhereSecond,
        OrderBy,
        Count,
        Average,
        Min,
        Max,
        Sum,
        First,
        Last,
        Take,
        Skip,
        TakeLast,
        SkipLast,
        GroupBy,
        Having,
        Distinct,
        Union,
        UnionAll,
        Except,
    }

    class CryEntityExpressionVisitor : ExpressionVisitor
    {
        public CryEntityQueryStatement QueryStatement { get; private set; }

        private CryEntityQueryMethod m_currentMethod;
        private CryEntityExpressionJoin m_currJoin;
        private CryEntityQueryWhereOnInfo m_currWhere;
        private CryEntityOrderInfo m_currOrderBy;
        private CryEntityGroupInfo m_currGroupInfo;
        private CryEntityQueryStatement m_currStatement;
        private PropertyInfo m_currMember;
        private int m_currIndex;
        private int m_unkNumber;
        private bool m_isConstant;

        public CryEntityExpressionVisitor()
        {
            m_currIndex = 0;
            m_unkNumber = 0;
        }

        protected override Expression VisitMethodCall(MethodCallExpression node)
        {
            if (node.Method.Name == "Select")
            {
                SetTableInfo(node);

                if (QueryStatement == null)
                    return base.VisitMethodCall(node);

                VisitSelect(node);
            }
            else if (node.Method.Name == "Join")
            {
                SetTableInfo(node);
                VisitJoin(node);
            }
            else if (node.Method.Name == "Where")
            {
                SetTableInfo(node);

                if (node.Arguments[0] is MethodCallExpression method)
                {
                    if (method.Method.Name == "GroupBy")
                    {
                        VisitHaving(node);
                        return node;
                    }
                }
                else
                    VisitWhere(node);
            }
            else if (node.Method.Name.Contains("OrderBy"))
            {
                SetTableInfo(node);

                if (QueryStatement == null)
                {
                    base.VisitMethodCall(node);
                    VisitOrderBy(node, node.Method.Name.Contains("Descending") ? CryEntityOrderDirection.Descending : CryEntityOrderDirection.Ascending);
                    return node;
                }

                VisitOrderBy(node, node.Method.Name.Contains("Descending") ? CryEntityOrderDirection.Descending : CryEntityOrderDirection.Ascending);
            }
            else if (node.Method.Name.Contains("Count"))
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitCount(node);
                return node;
            }
            else if (node.Method.Name == "Average")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitAverage(node);
                return node;
            }
            else if (node.Method.Name == "Min")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitMin(node);
                return node;
            }
            else if (node.Method.Name == "Max")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitMax(node);
                return node;
            }
            else if (node.Method.Name == "Sum")
            {
                base.VisitMethodCall(node);
                VisitSum(node);
                return node;
            }
            else if (node.Method.Name == "Take")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitTake(node);
                return node;
            }
            else if (node.Method.Name == "Skip")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitSkip(node);
                return node;
            }
            else if (node.Method.Name == "TakeLast")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitTakeLast(node);
                return node;
            }
            else if (node.Method.Name == "SkipLast")
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitSkipLast(node);
                return node;
            }
            else if (node.Method.Name == "GroupBy")
            {
                SetTableInfo(node);
                VisitGroupBy(node);
            }
            else if (node.Method.Name == "Distinct")
            {
                SetTableInfo(node);
                VisitDistinct(node);
            }
            else if (node.Method.Name == "Union")
            {
                VisitUnion(node);
                return node;
            }
            else if (node.Method.Name == "Concat")
            {
                VisitUnionAll(node);
                return node;
            }
            else if (node.Method.Name == "Except")
            {
                VisitExcept(node);
                return node;
            }
            else if (node.Method.Name.Contains("First"))
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitFirst(node);
                return node;
            }
            else if (node.Method.Name.Contains("Last"))
            {
                SetTableInfo(node);
                base.VisitMethodCall(node);
                VisitLast(node);
                return node;
            }

            return base.VisitMethodCall(node);
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            switch (m_currentMethod)
            {
                case CryEntityQueryMethod.Where:
                case CryEntityQueryMethod.Having:
                    {
                        if (node.NodeType == ExpressionType.AndAlso)
                        {
                            m_currWhere.NextOperator = CryEntityQueryLogicalOperator.And;
                            Visit(node.Left);
                            m_currWhere.Next = new CryEntityQueryWhereOnInfo();
                            m_currWhere = m_currWhere.Next;
                            Visit(node.Right);
                        }
                        else if (node.NodeType == ExpressionType.OrElse)
                        {
                            m_currWhere.NextOperator = CryEntityQueryLogicalOperator.Or;
                            Visit(node.Left);
                            m_currWhere.Next = new CryEntityQueryWhereOnInfo();
                            m_currWhere = m_currWhere.Next;
                            Visit(node.Right);
                        }
                        else
                        {
                            m_currWhere.Operator = GetOperator(node.NodeType);
                            VisitWhereFirst(node.Left);
                            VisitWhereSecond(node.Right);
                        }

                        return node;
                    }
            }

            return base.VisitBinary(node);
        }

        protected override Expression VisitConstant(ConstantExpression node)
        {
            m_isConstant = true;

            switch (m_currentMethod)
            {
                case CryEntityQueryMethod.WhereFirst:
                    {
                        if (CryEntityQueryableProvider.IsAnonymousType(node.Type))
                        {
                            var lastWhere = m_currWhere;

                            var properties = node.Value.GetType().GetProperties();

                            m_currWhere.Second = node.Value;

                            foreach (var prop in properties)
                            {
                                if (m_currWhere.First != null)
                                {
                                    if (m_currWhere.Next == null)
                                        m_currWhere.Next = new CryEntityQueryWhereOnInfo();

                                    m_currWhere = m_currWhere.Next;
                                }

                                m_currWhere.First = prop.GetValue(node.Value);
                            }

                            m_currWhere = lastWhere;
                            return node;
                        }

                        m_currWhere.First = node.Value;
                        break;
                    }
                case CryEntityQueryMethod.WhereSecond:
                    {
                        if (CryEntityQueryableProvider.IsAnonymousType(node.Type))
                        {
                            var lastWhere = m_currWhere;

                            var properties = node.Value.GetType().GetProperties();

                            m_currWhere.Second = node.Value;

                            foreach (var prop in properties)
                            {
                                if (m_currWhere.Second != null)
                                {
                                    if (m_currWhere.Next == null)
                                        m_currWhere.Next = new CryEntityQueryWhereOnInfo();

                                    m_currWhere = m_currWhere.Next;
                                }

                                m_currWhere.Second = prop.GetValue(node.Value);
                            }

                            m_currWhere = lastWhere;
                            return node;
                        }

                        m_currWhere.Second = node.Value;
                        break;
                    }
                case CryEntityQueryMethod.Take:
                    {
                        m_currStatement.QueryInfo.Limit = (long)Convert.ChangeType(node.Value, typeof(long)) - m_currStatement.QueryInfo.Offset;
                        break;
                    }
                case CryEntityQueryMethod.Skip:
                    {
                        m_currStatement.QueryInfo.Offset = (long)Convert.ChangeType(node.Value, typeof(long));

                        if (m_currStatement.QueryInfo.Limit != 0)
                            m_currStatement.QueryInfo.Limit -= m_currStatement.QueryInfo.Offset;

                        break;
                    }
                case CryEntityQueryMethod.TakeLast:
                    {
                        if (m_currStatement.QueryInfo.Limit != 0)
                        {
                            var value = (long)Convert.ChangeType(node.Value, typeof(long));

                            if (m_currStatement.QueryInfo.Offset != 0)
                            {
                                var limit = m_currStatement.QueryInfo.Offset + m_currStatement.QueryInfo.Limit;

                                m_currStatement.QueryInfo.Offset = limit - value;
                                m_currStatement.QueryInfo.Limit = limit - m_currStatement.QueryInfo.Offset;
                            }
                            else
                            {
                                m_currStatement.QueryInfo.Offset = m_currStatement.QueryInfo.Limit - value - 1;
                                m_currStatement.QueryInfo.Limit = value;
                            }
                        }
                        else
                        {
                            m_currStatement.QueryInfo.Limit = (long)Convert.ChangeType(node.Value, typeof(long));

                            if (m_currStatement.QueryInfo.Offset == 0)
                                m_currStatement.QueryInfo.Offset = -1;
                        }

                        break;
                    }
                case CryEntityQueryMethod.SkipLast:
                    {
                        if (m_currStatement.QueryInfo.Limit != 0)
                        {
                            m_currStatement.QueryInfo.Limit -= (long)Convert.ChangeType(node.Value, typeof(long));
                        }
                        else
                        {
                            m_currStatement.QueryInfo.Limit = -(long)Convert.ChangeType(node.Value, typeof(long));

                            if (m_currStatement.QueryInfo.Offset == 0)
                                m_currStatement.QueryInfo.Offset = -1;
                        }

                        break;
                    }
            }

            return base.VisitConstant(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            switch (m_currentMethod)
            {
                case CryEntityQueryMethod.Select:
                    {
                        if (node.Member is PropertyInfo propertyInfo)
                        {
                            var tableInfo = CryEntityManager.CreateOrGetTableInfo(propertyInfo.DeclaringType);

                            if (tableInfo == null)
                                break;

                            var column = tableInfo.GetColumn(propertyInfo);

                            if (column != null)
                                m_currStatement.QueryInfo.Columns.Add(new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ });
                        }

                        break;
                    }
                case CryEntityQueryMethod.JoinSource:
                    {
                        if (node.Member is PropertyInfo propertyInfo)
                        {
                            var column = m_currStatement.QueryInfo.TableInfo.GetColumn(propertyInfo);

                            if (column != null)
                                m_currJoin.OnInfo.First = new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ };
                        }
                        break;
                    }
                case CryEntityQueryMethod.JoinDest:
                    {
                        if (node.Member is PropertyInfo propertyInfo)
                        {
                            var column = m_currJoin.TableInfo.GetColumn(propertyInfo);

                            if (column != null)
                                m_currJoin.OnInfo.Second = new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ };
                        }
                        break;
                    }
                case CryEntityQueryMethod.WhereFirst:
                    {
                        m_isConstant = false;
                        Visit(node.Expression);

                        if (CryEntityQueryableProvider.IsAnonymousType(m_currWhere.First.GetType()))
                        {
                            if (node.Member.MemberType == MemberTypes.Property)
                                m_currWhere.First = (node.Member as PropertyInfo).GetValue(m_currWhere.First);
                            else if (node.Member.MemberType == MemberTypes.Field)
                                m_currWhere.First = (node.Member as FieldInfo).GetValue(m_currWhere.First);
                        }
                        else if (m_isConstant)
                        {
                            if (node.Member.MemberType == MemberTypes.Property)
                                m_currWhere.First = (node.Member as PropertyInfo).GetValue(m_currWhere.First);
                            else if (node.Member.MemberType == MemberTypes.Field)
                                m_currWhere.First = (node.Member as FieldInfo).GetValue(m_currWhere.First);
                        }
                        else if (node.Member is PropertyInfo propertyInfo)
                        {
                            var tableInfo = CryEntityManager.CreateOrGetTableInfo(propertyInfo.DeclaringType);

                            if (tableInfo == null)
                                break;

                            var column = tableInfo.GetColumn(propertyInfo);

                            if (column != null)
                                m_currWhere.First = new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ };
                        }

                        break;
                    }
                case CryEntityQueryMethod.WhereSecond:
                    {
                        m_isConstant = false;
                        Visit(node.Expression);

                        if (CryEntityQueryableProvider.IsAnonymousType(m_currWhere.Second.GetType()))
                        {
                            if (node.Member.MemberType == MemberTypes.Property)
                                m_currWhere.Second = (node.Member as PropertyInfo).GetValue(m_currWhere.Second);
                            else if (node.Member.MemberType == MemberTypes.Field)
                                m_currWhere.Second = (node.Member as FieldInfo).GetValue(m_currWhere.Second);
                        }
                        else if (m_isConstant)
                        {
                            if (node.Member.MemberType == MemberTypes.Property)
                                m_currWhere.Second = (node.Member as PropertyInfo).GetValue(m_currWhere.Second);
                            else if (node.Member.MemberType == MemberTypes.Field)
                                m_currWhere.Second = (node.Member as FieldInfo).GetValue(m_currWhere.Second);
                        }
                        else if (node.Member is PropertyInfo propertyInfo)
                        {
                            var tableInfo = CryEntityManager.CreateOrGetTableInfo(propertyInfo.DeclaringType);

                            if (tableInfo == null)
                                break;

                            var column = tableInfo.GetColumn(propertyInfo);

                            if (column != null)
                                m_currWhere.Second = new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ };
                        }

                        return node;
                    }
                case CryEntityQueryMethod.OrderBy:
                    {
                        if (node.Member is PropertyInfo propertyInfo)
                        {
                            var tableInfo = CryEntityManager.CreateOrGetTableInfo(propertyInfo.DeclaringType);

                            if (tableInfo == null)
                                break;

                            var column = tableInfo.GetColumn(propertyInfo);

                            if (column != null)
                            {
                                if (m_currOrderBy.Column != null)
                                {
                                    m_currOrderBy.Next = new CryEntityOrderInfo(m_currOrderBy.OrderDirection);
                                    m_currOrderBy = m_currOrderBy.Next;
                                }

                                m_currOrderBy.Column = new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ };
                            }
                        }

                        break;
                    }
                case CryEntityQueryMethod.GroupBy:
                    {
                        if (node.Member is PropertyInfo propertyInfo)
                        {
                            var tableInfo = CryEntityManager.CreateOrGetTableInfo(propertyInfo.DeclaringType);

                            if (tableInfo == null)
                                break;

                            var column = tableInfo.GetColumn(propertyInfo);

                            if (column != null)
                            {
                                if (m_currGroupInfo.ColumnInfo != null)
                                {
                                    m_currGroupInfo.Next = new CryEntityGroupByExpressionInfo();
                                    m_currGroupInfo = m_currGroupInfo.Next;
                                }

                                ((CryEntityGroupByExpressionInfo)m_currGroupInfo).Key = node.Type;

                                m_currGroupInfo.ColumnInfo = new CryEntityCreateQueryColumnInfo(column) { Index = m_currIndex++ };
                            }
                        }

                        break;
                    }
                case CryEntityQueryMethod.Count:
                case CryEntityQueryMethod.Average:
                case CryEntityQueryMethod.Min:
                case CryEntityQueryMethod.Max:
                case CryEntityQueryMethod.Sum:
                case CryEntityQueryMethod.First:
                case CryEntityQueryMethod.Last:
                    {
                        if (node.Member is PropertyInfo propertyInfo)
                        {
                            var tableInfo = CryEntityManager.CreateOrGetTableInfo(propertyInfo.DeclaringType);

                            if (tableInfo == null)
                                break;

                            var column = tableInfo.GetColumn(propertyInfo);

                            if (column != null)
                                m_currStatement.QueryInfo.Columns.Add(new CryEntityCreateQueryColumnInfo(column, GetManipulator(m_currentMethod)) { Index = m_currIndex++ });
                        }

                        break;
                    }
            }

            return base.VisitMember(node);
        }

        protected override Expression VisitNew(NewExpression node)
        {
            if (node.Arguments.Count == node.Members.Count && CryEntityQueryableProvider.IsAnonymousType(node.Type))
            {
                CryEntityQueryWhereOnInfo lastWhere = m_currWhere;

                for (int i = 0; i < node.Arguments.Count; ++i)
                {
                    if (m_currentMethod == CryEntityQueryMethod.WhereFirst)
                    {
                        if (m_currWhere.First != null)
                        {
                            if (m_currWhere.Next == null)
                                m_currWhere.Next = new CryEntityQueryWhereOnInfo();

                            m_currWhere = m_currWhere.Next;
                        }
                    }
                    else if (m_currentMethod == CryEntityQueryMethod.WhereSecond)
                    {
                        if (m_currWhere.Second != null)
                        {
                            if (m_currWhere.Next == null)
                                m_currWhere.Next = new CryEntityQueryWhereOnInfo();

                            m_currWhere = m_currWhere.Next;
                        }
                    }
                    else
                        m_currMember = node.Members[i] as PropertyInfo;

                    Visit(node.Arguments[i]);
                }

                m_currWhere = lastWhere;
            }

            return node;
        }

        private void SetTableInfo(MethodCallExpression expression)
        {
            if (QueryStatement == null)
            {
                if (CryEntityQueryableProvider.TryGetElementType(expression.Arguments[0].Type, out var type))
                {
                    if (type.GetCustomAttribute<CryEntityTableNameAttribute>() != null)
                    {
                        QueryStatement = new CryEntityQueryStatement();
                        m_currStatement = QueryStatement;
                        m_currStatement.QueryInfo = new CryEntityCreateQueryInfo(CryEntityManager.CreateOrGetTableInfo(type));
                    }
                }
            }
        }

        private void VisitSelect(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Select;

            Visit(expression.Arguments[expression.Arguments.Count - 1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitJoin(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.JoinSource;

            m_currJoin = new CryEntityExpressionJoin
            {
                OnInfo = new CryEntityQueryWhereOnInfo()
            };

            m_currJoin.OnInfo.Operator = CryEntityQueryOperator.Equal;

            if (CryEntityQueryableProvider.TryGetElementType(expression.Arguments[1].Type, out var type))
                m_currJoin.TableInfo = CryEntityManager.CreateOrGetTableInfo(type);

            Visit(expression.Arguments[2]);

            m_currentMethod = CryEntityQueryMethod.JoinDest;

            Visit(expression.Arguments[3]);

            m_currStatement.QueryInfo.Joins.Add(new CryEntityQueryJoinInfo(m_currJoin.TableInfo, m_currJoin.OnInfo));

            m_currJoin = null;
            m_currentMethod = CryEntityQueryMethod.None;

            VisitSelect(expression);
        }

        private void VisitWhere(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Where;

            m_currStatement.QueryInfo.Where = new CryEntityQueryWhereOnInfo();
            m_currWhere = m_currStatement.QueryInfo.Where;

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitOrderBy(MethodCallExpression expression, CryEntityOrderDirection direction)
        {
            m_currentMethod = CryEntityQueryMethod.OrderBy;

            if (m_currStatement.QueryInfo.OrderBy == null)
            {
                m_currStatement.QueryInfo.OrderBy = new CryEntityOrderInfo(direction);
                m_currOrderBy = m_currStatement.QueryInfo.OrderBy;
            }
            else
            {
                m_currOrderBy.Next = new CryEntityOrderInfo(direction);
                m_currOrderBy = m_currStatement.QueryInfo.OrderBy.Next;
            }

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitWhereFirst(Expression expression)
        {
            m_currentMethod = CryEntityQueryMethod.WhereFirst;

            Visit(expression);

            m_currentMethod = CryEntityQueryMethod.Where;
        }

        private void VisitWhereSecond(Expression expression)
        {
            m_currentMethod = CryEntityQueryMethod.WhereSecond;

            Visit(expression);

            m_currentMethod = CryEntityQueryMethod.Where;
        }

        private void VisitCount(MethodCallExpression expression)
        {
            if (m_currentMethod == CryEntityQueryMethod.WhereFirst)
                m_currWhere.First = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Count);
            else if (m_currentMethod == CryEntityQueryMethod.WhereSecond)
                m_currWhere.Second = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Count);
            else
            {
                m_currentMethod = CryEntityQueryMethod.Count;

                if (expression.Arguments.Count > 1)
                    Visit(expression.Arguments[1]);
                else if (m_currMember != null)
                {
                    if (m_currStatement.QueryInfo.GroupBy != null)
                        m_currStatement.QueryInfo.GroupBy.AdditionalColumns.Add(new CryEntityUnknownColumnInfo("grpCount" + m_currMember.Name, CryEntityColumnManipulator.Count) { Index = m_currIndex++, PropertyType = m_currMember.PropertyType });
                    else
                        m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Count;
                }
                else
                    m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Count;

                m_currentMethod = CryEntityQueryMethod.None;
            }
        }

        private void VisitAverage(MethodCallExpression expression)
        {
            if (m_currentMethod == CryEntityQueryMethod.WhereFirst)
                m_currWhere.First = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Average);
            else if (m_currentMethod == CryEntityQueryMethod.WhereSecond)
                m_currWhere.Second = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Average);
            else
            {
                m_currentMethod = CryEntityQueryMethod.Average;

                if (expression.Arguments.Count > 1)
                    Visit(expression.Arguments[1]);
                else if (m_currMember != null)
                {
                    if (m_currStatement.QueryInfo.GroupBy != null)
                        m_currStatement.QueryInfo.GroupBy.AdditionalColumns.Add(new CryEntityUnknownColumnInfo("grpAvg" + m_currMember.Name, CryEntityColumnManipulator.Average) { Index = m_currIndex++, PropertyType = m_currMember.PropertyType });
                    else
                        m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Average;
                }
                else
                    m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Average;

                m_currentMethod = CryEntityQueryMethod.None;
            }
        }

        private void VisitMin(MethodCallExpression expression)
        {
            if (m_currentMethod == CryEntityQueryMethod.WhereFirst)
                m_currWhere.First = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Min);
            else if (m_currentMethod == CryEntityQueryMethod.WhereSecond)
                m_currWhere.Second = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Min);
            else
            {
                m_currentMethod = CryEntityQueryMethod.Min;

                if (expression.Arguments.Count > 1)
                    Visit(expression.Arguments[1]);
                else if (m_currMember != null)
                {
                    if (m_currStatement.QueryInfo.GroupBy != null)
                        m_currStatement.QueryInfo.GroupBy.AdditionalColumns.Add(new CryEntityUnknownColumnInfo("grpMin" + m_currMember.Name, CryEntityColumnManipulator.Min) { Index = m_currIndex++, PropertyType = m_currMember.PropertyType });
                    else
                        m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Min;
                }
                else
                    m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Min;

                m_currentMethod = CryEntityQueryMethod.None;
            }
        }

        private void VisitMax(MethodCallExpression expression)
        {
            if (m_currentMethod == CryEntityQueryMethod.WhereFirst)
                m_currWhere.First = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Max);
            else if (m_currentMethod == CryEntityQueryMethod.WhereSecond)
                m_currWhere.Second = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Max);
            else
            {
                m_currentMethod = CryEntityQueryMethod.Max;

                if (expression.Arguments.Count > 1)
                    Visit(expression.Arguments[1]);
                else if (m_currMember != null)
                {
                    if (m_currStatement.QueryInfo.GroupBy != null)
                        m_currStatement.QueryInfo.GroupBy.AdditionalColumns.Add(new CryEntityUnknownColumnInfo("grpMax" + m_currMember.Name, CryEntityColumnManipulator.Max) { Index = m_currIndex++, PropertyType = m_currMember.PropertyType });
                    else
                        m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Max;
                }
                else
                    m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Max;

                m_currentMethod = CryEntityQueryMethod.None;
            }
        }

        private void VisitSum(MethodCallExpression expression)
        {
            if (m_currentMethod == CryEntityQueryMethod.WhereFirst)
                m_currWhere.First = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Sum);
            else if (m_currentMethod == CryEntityQueryMethod.WhereSecond)
                m_currWhere.Second = new CryEntityUnknownColumnInfo($"unk{m_unkNumber++}", CryEntityColumnManipulator.Sum);
            else
            {
                m_currentMethod = CryEntityQueryMethod.Sum;

                if (expression.Arguments.Count > 1)
                    Visit(expression.Arguments[1]);
                else if (m_currMember != null)
                {
                    if (m_currStatement.QueryInfo.GroupBy != null)
                        m_currStatement.QueryInfo.GroupBy.AdditionalColumns.Add(new CryEntityUnknownColumnInfo("grpSum" + m_currMember.Name, CryEntityColumnManipulator.Sum) { Index = m_currIndex++, PropertyType = m_currMember.PropertyType });
                    else
                        m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Sum;
                }
                else
                    m_currStatement.QueryInfo.Manipulator = CryEntityColumnManipulator.Sum;

                m_currentMethod = CryEntityQueryMethod.None;
            }
        }

        private void VisitTake(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Take;

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitSkip(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Skip;

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitTakeLast(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.TakeLast;

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitSkipLast(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.SkipLast;

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitGroupBy(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.GroupBy;

            if (m_currStatement.QueryInfo.GroupBy == null)
            {
                m_currStatement.QueryInfo.GroupBy = new CryEntityGroupHavingInfo
                {
                    GroupInfo = new CryEntityGroupByExpressionInfo()
                };

                m_currGroupInfo = m_currStatement.QueryInfo.GroupBy.GroupInfo;
            }
            else
            {
                m_currGroupInfo.Next = new CryEntityGroupByExpressionInfo();
                m_currGroupInfo = m_currGroupInfo.Next;
            }

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitHaving(MethodCallExpression expression)
        {
            Visit(expression.Arguments[0]);

            m_currentMethod = CryEntityQueryMethod.Having;
            m_currStatement.QueryInfo.GroupBy.Having = new CryEntityQueryWhereOnInfo();
            m_currWhere = m_currStatement.QueryInfo.GroupBy.Having;

            Visit(expression.Arguments[1]);

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitDistinct(MethodCallExpression _)
        {
            m_currentMethod = CryEntityQueryMethod.Distinct;

            m_currStatement.QueryInfo.IsDistinct = true;

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitUnion(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Union;

            Visit(expression.Arguments[0]);

            if (CryEntityQueryableProvider.TryGetElementType(expression.Arguments[0].Type, out var type))
            {
                CryEntityTableInfo tableInfo = CryEntityManager.CreateOrGetTableInfo(type);

                m_currStatement.IsUnion = true;
                m_currStatement.Next = new CryEntityQueryStatement();
                m_currStatement = m_currStatement.Next;
                m_currStatement.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);

                m_currentMethod = CryEntityQueryMethod.Union;

                Visit(expression.Arguments[1]);
            }

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitUnionAll(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.UnionAll;

            Visit(expression.Arguments[0]);

            if (CryEntityQueryableProvider.TryGetElementType(expression.Arguments[0].Type, out var type))
            {
                CryEntityTableInfo tableInfo = CryEntityManager.CreateOrGetTableInfo(type);

                m_currStatement.IsUnionAll = true;
                m_currStatement.Next = new CryEntityQueryStatement();
                m_currStatement = m_currStatement.Next;
                m_currStatement.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);

                m_currentMethod = CryEntityQueryMethod.UnionAll;

                Visit(expression.Arguments[1]);
            }

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitExcept(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Except;

            Visit(expression.Arguments[0]);

            if (CryEntityQueryableProvider.TryGetElementType(expression.Arguments[0].Type, out var type))
            {
                CryEntityTableInfo tableInfo = CryEntityManager.CreateOrGetTableInfo(type);

                m_currStatement.IsExcept = true;
                m_currStatement.Next = new CryEntityQueryStatement();
                m_currStatement = m_currStatement.Next;
                m_currStatement.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);

                m_currentMethod = CryEntityQueryMethod.Except;

                Visit(expression.Arguments[1]);
            }

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitFirst(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.First;

            if (expression.Arguments.Count > 1)
                Visit(expression.Arguments[1]);
            else
            {
                if (m_currStatement.QueryInfo.Offset != 0)
                    m_currStatement.QueryInfo.Limit = 1;
                else
                {
                    m_currStatement.QueryInfo.Limit = 1;
                    m_currStatement.QueryInfo.Offset = 0;
                }
            }

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private void VisitLast(MethodCallExpression expression)
        {
            m_currentMethod = CryEntityQueryMethod.Last;

            if (expression.Arguments.Count > 1)
                Visit(expression.Arguments[1]);
            else
            {
                if (m_currStatement.QueryInfo.Limit != 0)
                {
                    m_currStatement.QueryInfo.Offset = m_currStatement.QueryInfo.Limit - 1;
                    m_currStatement.QueryInfo.Limit = 1;
                }
                else
                {
                    m_currStatement.QueryInfo.Offset = -1;
                    m_currStatement.QueryInfo.Limit = 1;
                }
            }

            m_currentMethod = CryEntityQueryMethod.None;
        }

        private static CryEntityQueryOperator GetOperator(ExpressionType expressionType)
        {
            switch (expressionType)
            {
                case ExpressionType.Equal:
                    return CryEntityQueryOperator.Equal;
                case ExpressionType.GreaterThan:
                    return CryEntityQueryOperator.GreaterThen;
                case ExpressionType.GreaterThanOrEqual:
                    return CryEntityQueryOperator.GreaterOrEqual;
                case ExpressionType.LessThan:
                    return CryEntityQueryOperator.LesserThen;
                case ExpressionType.LessThanOrEqual:
                    return CryEntityQueryOperator.LesserOrEqual;
                case ExpressionType.Not:
                    return CryEntityQueryOperator.IsNot;
                case ExpressionType.NotEqual:
                    return CryEntityQueryOperator.NotEqual;
                case ExpressionType.IsTrue:
                    return CryEntityQueryOperator.Is;
                case ExpressionType.IsFalse:
                    return CryEntityQueryOperator.IsNot;
                default:
                    break;
            }

            throw new ArgumentException("Invalid expression type", nameof(expressionType));
        }

        private static CryEntityColumnManipulator GetManipulator(CryEntityQueryMethod queryMethod)
        {
            switch (queryMethod)
            {
                case CryEntityQueryMethod.Count:
                    return CryEntityColumnManipulator.Count;
                case CryEntityQueryMethod.Average:
                    return CryEntityColumnManipulator.Average;
                case CryEntityQueryMethod.Min:
                    return CryEntityColumnManipulator.Min;
                case CryEntityQueryMethod.Max:
                    return CryEntityColumnManipulator.Max;
                case CryEntityQueryMethod.Sum:
                    return CryEntityColumnManipulator.Sum;
                default:
                    break;
            }

            throw new ArgumentException("Invalid query method", nameof(queryMethod));
        }

    }
}
