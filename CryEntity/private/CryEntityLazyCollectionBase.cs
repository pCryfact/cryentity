﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace CryEntity
{
    abstract class CryEntityLazyCollectionBase<T> : ICryEntityLazyCollection<T>
    {
        class MyEnumerator : IEnumerator<T>
        {
            public T Current { get; set; }

            object IEnumerator.Current => Current;

            private readonly CryEntityLazyCollectionBase<T> m_collection;
            private int m_offset;

            public MyEnumerator(CryEntityLazyCollectionBase<T> collection)
            {
                m_collection = collection;
                m_offset = 0;
            }

            public void Dispose()
            { }

            public bool MoveNext()
            {
                if (m_offset < m_collection.m_myLimit)
                {
                    if (m_offset < m_collection.InternalList.Count)
                    {
                        Current = m_collection.InternalList[m_offset++];
                        return true;
                    }
                    else
                    {
                        m_collection.Pull();

                        if (m_offset < m_collection.InternalList.Count)
                        {
                            Current = m_collection.InternalList[m_offset++];
                            return true;
                        }
                    }
                }

                Current = default;
                return false;
            }

            public void Reset()
            {
                m_offset = 0;
            }
        }

        class MyEnumeratorTaskPull : IEnumerator<T>
        {
            public T Current { get; set; }

            object IEnumerator.Current => Current;

            private readonly CryEntityLazyCollectionBase<T> m_collection;
            private int m_offset;

            public MyEnumeratorTaskPull(CryEntityLazyCollectionBase<T> collection)
            {
                collection.m_manualResetEvent.Reset();

                m_collection = collection;
                m_offset = 0;
            }

            public void Dispose()
            { }

            public bool MoveNext()
            {
                if (m_offset < m_collection.m_myLimit)
                {
                    if (m_offset < m_collection.InternalList.Count)
                    {
                        Current = m_collection.InternalList[m_offset++];
                        return true;
                    }
                    else if (m_collection.m_manualResetEvent.WaitOne(TimeSpan.FromSeconds(2))) // wait max 2 seconds for new data
                    {
                        m_collection.m_manualResetEvent.Reset();

                        if (m_offset < m_collection.InternalList.Count)
                        {
                            Current = m_collection.InternalList[m_offset++];
                            return true;
                        }
                    }
                }

                Current = default;
                return false;
            }

            public void Reset()
            {
                m_offset = 0;
            }
        }

        private readonly int m_myLimit;
        private readonly ManualResetEvent m_manualResetEvent;
        private Task m_pullTask;
        private bool m_autoPulling;

        public int Count => m_myLimit;
        public int LoadedCount => InternalList.Count;
        public bool IsReadOnly => false;

        public bool AutoPulling
        {
            get => m_autoPulling;
            set
            {
                if (!m_autoPulling && value)
                {
                    m_autoPulling = value;

                    m_manualResetEvent.Reset();
                    m_pullTask = new Task(ProcessPulling);
                    m_pullTask.Start();
                }
                else if (m_autoPulling && !value)
                {
                    if (!m_pullTask.IsCompleted)
                    {
                        m_autoPulling = value;
                        m_pullTask?.Wait();
                        m_pullTask = null;
                    }
                }
            }
        }

        public bool IsAutoPullFinished { get; private set; }

        protected CryEntityQueryStatement QueryCreateInfo { get; }
        protected DbConnection DbConnection { get; }
        protected ICryEntityQueryBuilder QueryBuilder { get; }

        protected IList<T> InternalList;

        public CryEntityLazyCollectionBase(DbConnection connection, ICryEntityQueryBuilder queryBuilder,
            CryEntityQueryStatement queryCreateInfo, int realLimit, Type prefferedList)
        {
            if (realLimit == 0)
            {
                if (queryCreateInfo.QueryInfo.Limit != 0)
                    m_myLimit = (int)queryCreateInfo.QueryInfo.Limit;
                else
                {
                    using var cmd = connection.CreateCommand();
                    var oldManipulator = queryCreateInfo.QueryInfo.Manipulator;
                    queryCreateInfo.QueryInfo.Manipulator = CryEntityColumnManipulator.Count;

                    if (queryCreateInfo.QueryInfo.Limit == 0)
                        queryCreateInfo.QueryInfo.Limit = CryEntityManager.MaxPullCount;

                    if (queryCreateInfo.QueryInfo.Command != null)
                        queryCreateInfo.QueryInfo.Command.Dispose();

                    var oldLimit = queryCreateInfo.QueryInfo.Limit;
                    var oldOffset = queryCreateInfo.QueryInfo.Offset;

                    queryCreateInfo.QueryInfo.Command = cmd;
                    queryCreateInfo.QueryInfo.Limit = 0;
                    queryCreateInfo.QueryInfo.Offset = 0;

                    cmd.CommandText = queryBuilder.CreateSelectQuery(queryCreateInfo);
                    queryCreateInfo.QueryInfo.Manipulator = oldManipulator;
                    queryCreateInfo.QueryInfo.Limit = oldLimit;
                    queryCreateInfo.QueryInfo.Offset = oldOffset;

                    m_myLimit = (int)(long)cmd.ExecuteScalar();
                }
            }
            else
                m_myLimit = realLimit;

            QueryCreateInfo = queryCreateInfo;
            DbConnection = connection;
            QueryBuilder = queryBuilder;
            InternalList = (IList<T>)Activator.CreateInstance(prefferedList.MakeGenericType(typeof(T)));
            m_manualResetEvent = new ManualResetEvent(false);
        }

        public void Add(T item)
        {
            InternalList.Add(item);
        }

        public void Clear()
        {
            InternalList.Clear();
        }

        public bool Contains(T item)
        {
            return InternalList.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            InternalList.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (Count - LoadedCount == 0)
                return InternalList.GetEnumerator();

            return AutoPulling ? new MyEnumeratorTaskPull(this) : new MyEnumerator(this);
        }

        public bool Remove(T item)
        {
            return InternalList.Remove(item);
        }

        public abstract void Pull();

        public void WaitForAutoPullingFinished()
        {
            if (AutoPulling)
            {
                m_pullTask?.Wait();
                m_pullTask = null;
            }
        }

        private void ProcessPulling()
        {
            int lastCount = LoadedCount;
            IsAutoPullFinished = false;

            while (AutoPulling && Count - LoadedCount > 0)
            {
                Pull();

                m_manualResetEvent.Set();

                if (lastCount == LoadedCount)
                    break;
            }

            IsAutoPullFinished = true;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
