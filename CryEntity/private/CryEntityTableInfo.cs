﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace CryEntity
{
    internal class CryEntityTableInfo
    {
        private static readonly MethodInfo ms_DbReaderRead = typeof(DbDataReader).GetMethod("Read");

        private static readonly ParameterExpression ms_dbReader = Expression.Parameter(typeof(DbDataReader), "dbReader");
        private static readonly ParameterExpression ms_dict = Expression.Parameter(typeof(Dictionary<CryEntityColumnInfo, object>), "dict");
        private static readonly MethodInfo ms_dictAdd = typeof(Dictionary<CryEntityColumnInfo, object>).GetMethod("Add");

        private static readonly MethodInfo ms_objectDictAdd = typeof(Dictionary<object, object>).GetMethod("Add");
        private static readonly MethodInfo ms_objectDictAddContainsKey = typeof(Dictionary<object, object>).GetMethod("ContainsKey");
        private static readonly MethodInfo ms_columnDictAdd = typeof(Dictionary<string, Dictionary<object, object>>).GetMethod("Add");
        private static readonly MethodInfo ms_columnDictTryGetValue = typeof(Dictionary<string, Dictionary<object, object>>).GetMethod("TryGetValue");

        private delegate void FillObjectDelegate<T>(DbDataReader reader, ICollection<T> output, bool allowCopy, Dictionary<object, KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>> dict);

        private delegate void GetValuesDelegate<T>(T obj, Dictionary<CryEntityColumnInfo, object> dict);

        public Type Type { get; }
        public string Name { get; }

        public CryEntityColumnInfo PrimaryKey { get; }
        public List<CryEntityForeignKeyInfo> ForeignKeys { get; }
        public List<CryEntityColumnInfo> Ignored { get; }
        public Dictionary<string, CryEntityColumnInfo> Columns { get; }
        public CryEntityTableInfo Base { get; }
        public List<CryEntityTableReferenceInfo> ReferencedTables { get; }

        public Delegate FillMethod { get; private set; }
        public Delegate GetValuesMethod { get; private set; }

        public CryEntityTableInfo(Type type)
        {
            Columns = new Dictionary<string, CryEntityColumnInfo>();
            ForeignKeys = new List<CryEntityForeignKeyInfo>();
            Ignored = new List<CryEntityColumnInfo>();
            ReferencedTables = new List<CryEntityTableReferenceInfo>();
            Type = type;

            var tableNameAttr = (CryEntityTableNameAttribute)Attribute.GetCustomAttribute(type, typeof(CryEntityTableNameAttribute));

            if (tableNameAttr == null)
                Name = type.Name;
            else
                Name = tableNameAttr.Name;

            if (type.BaseType != typeof(object))
            {
                var foreignAttr = (CryEntityForeignKeyAttribute)Attribute.GetCustomAttribute(type, typeof(CryEntityForeignKeyAttribute));

                if (foreignAttr != null)
                {
                    Base = new CryEntityTableInfo(type.BaseType);
                    ForeignKeys.Add(new CryEntityForeignKeyInfo(new CryEntityColumnInfo(this, foreignAttr), Base.PrimaryKey));

                    lock (CryEntityManager.TableInfo)
                    {
                        if (!CryEntityManager.TableInfo.ContainsKey(type.BaseType))
                            CryEntityManager.TableInfo.Add(type.BaseType, Base);
                    }
                }
            }
            else
                Base = null;

            var properties = Type.GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in properties)
            {
                if (!property.CanRead || !property.CanWrite || property.SetMethod.IsPrivate || property.GetMethod.IsPrivate)
                    continue;

                var column = new CryEntityColumnInfo(this, property);

                if (column.IsIgnored)
                    Ignored.Add(column);
                else
                {
                    if (column.IsPrimaryKey)
                        PrimaryKey = column;

                    if (column.IsForeignKey)
                    {
                        if ((CryEntityTableNameAttribute)Attribute.GetCustomAttribute(column.PropertyInfo.PropertyType, typeof(CryEntityTableNameAttribute)) != null)
                        {
                      //      column.ColumnName = column.ForeignKeyAttribute.ColumnName;

                            var table = new CryEntityTableInfo(column.PropertyInfo.PropertyType);
                            ReferencedTables.Add(new CryEntityTableReferenceInfo(table, column));
                            ForeignKeys.Add(new CryEntityForeignKeyInfo(column, table.PrimaryKey));
                        }
                        else
                        {
                            Type t = column.PropertyInfo.PropertyType;

                            if (t.IsGenericType)
                            {
                                var genericTypes = t.GetGenericArguments();
                                CryEntityForeignKeyInfo foreignKey = null;

                                if (genericTypes.Length == 1 && typeof(ICollection<>).MakeGenericType(genericTypes).IsAssignableFrom(t))
                                {
                                    if ((CryEntityTableNameAttribute)Attribute.GetCustomAttribute(genericTypes[0], typeof(CryEntityTableNameAttribute)) != null)
                                    {
                                        var table = CryEntityManager.CreateOrGetTableInfo(genericTypes[0]);

                                        foreignKey = new CryEntityForeignKeyInfo(column, table.PrimaryKey)
                                        {
                                            CollectionGenericTypes = genericTypes,
                                            IsCollection = true,
                                            RealCollectionType = t
                                        };

                                        if (t.IsAbstract)
                                            foreignKey.CollectionType = CryEntityManager.PreferredCollection.MakeGenericType(genericTypes);
                                        else
                                            foreignKey.CollectionType = t;

                                        ReferencedTables.Add(new CryEntityTableReferenceInfo(table, column, foreignKey));
                                    }
                                }
                                else if (genericTypes.Length == 2 && typeof(IDictionary<,>).MakeGenericType(genericTypes).IsAssignableFrom(t))
                                {
                                    if ((CryEntityTableNameAttribute)Attribute.GetCustomAttribute(genericTypes[1], typeof(CryEntityTableNameAttribute)) != null)
                                    {
                                        var table = CryEntityManager.CreateOrGetTableInfo(genericTypes[1]);

                                        foreignKey = new CryEntityForeignKeyInfo(column, table.PrimaryKey)
                                        {
                                            CollectionGenericTypes = genericTypes,
                                            IsDictionary = true,
                                            RealCollectionType = t
                                        };

                                        if (t.IsAbstract)
                                            foreignKey.CollectionType = CryEntityManager.PreferredDictionary.MakeGenericType(genericTypes);
                                        else
                                            foreignKey.CollectionType = t;

                                        ReferencedTables.Add(new CryEntityTableReferenceInfo(table, column, foreignKey));
                                    }
                                }

                                if (foreignKey != null)
                                    ForeignKeys.Add(foreignKey);
                            }
                        }
                    }
                    else
                    {
                       var foreignKey = GetColumnForeignkeyCollision(column);

                        if (foreignKey != null)
                        {
                            foreignKey.CopyFrom(column);
                            Columns.Add(foreignKey.ColumnName, foreignKey);
                        }
                        else
                            Columns.Add(column.ColumnName, column);
                    }
                }
            }
        }

        private CryEntityColumnInfo GetColumnForeignkeyCollision(CryEntityColumnInfo column)
        {
            foreach (var foreignKey in ForeignKeys)
            {
                if (foreignKey.ForeignKey.ColumnName == column.ColumnName)
                    return foreignKey.ForeignKey;
            }

            return null;
        }

        public void FillColumns<T>(DbDataReader reader, ICollection<T> output, bool allowCopy, Dictionary<object, KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>> dict)
        {
            KeyValuePair<T, Dictionary<string, Dictionary<object, object>>> obj = default;

            // try to find object otherwise create it
            foreach (var column in Columns)
            {
                if (column.Value.IsPrimaryKey)
                {
                    var tmp = reader[column.Value.Alias];

                    if (allowCopy)
                    {
                        obj = new KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>((T)Activator.CreateInstance(typeof(T)), new Dictionary<string, Dictionary<object, object>>());
                        output.Add(obj.Key);
                    }
                    else if (!dict.TryGetValue(tmp, out var obj2))
                    {
                        obj = new KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>((T)Activator.CreateInstance(typeof(T)), new Dictionary<string, Dictionary<object, object>>());
                        
                        dict.Add(tmp, obj);
                        output.Add(obj.Key);
                    }
                    else
                        obj = obj2;

                    column.Value.PropertyInfo.SetValue(obj.Key, tmp);
                    break;
                }
            }

            FillObject(this, reader, obj, obj.Key, true);
        }

        // used in reflection call
        public T FillTable<T>(DbDataReader reader, bool allowCopy)
        {
            T result = default;

            var dict =
                new Dictionary<object, KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>>();

            KeyValuePair<T, Dictionary<string, Dictionary<object, object>>> obj = default;

            // try to find object otherwise create it
            foreach (var column in Columns)
            {
                if (column.Value.IsPrimaryKey)
                {
                    var tmp = reader[column.Value.Alias];

                    if (allowCopy)
                    {
                        obj = new KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>((T)Activator.CreateInstance(typeof(T)), new Dictionary<string, Dictionary<object, object>>());
                        result = obj.Key;
                    }
                    else if (!dict.TryGetValue(tmp, out var obj2))
                    {
                        obj = new KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>((T)Activator.CreateInstance(typeof(T)), new Dictionary<string, Dictionary<object, object>>());
                        dict.Add(tmp, obj);
                        result = obj.Key;
                    }
                    else
                        obj = obj2;

                    column.Value.PropertyInfo.SetValue(obj.Key, tmp);
                    break;
                }
            }

            FillObject(this, reader, obj, obj.Key, true);
            return result;
        }

        public void Fill<T>(DbDataReader reader, ICollection<T> output, bool allowCopy)
        {
            var dict =
                new Dictionary<object, KeyValuePair<T, Dictionary<string, Dictionary<object, object>>>>();


            if (FillMethod == null)
            {
                bool isCompiled = false;

                // using task to run the slow version to load objects from database.
                Task task = Task.Run(() =>
                {
                    while (reader.Read())
                    {
                        FillColumns(reader, output, allowCopy, dict);

                        if (isCompiled)
                            break;
                    }
                });

                 CompileFillMethod<T>();

                isCompiled = true; // fast method compiled -> cancel the slow version and use the fast method
                task.Wait();
            }

            FillMethod.DynamicInvoke(reader, output, allowCopy, dict);
        }

        public void GetValues(object obj, Type type, Dictionary<CryEntityColumnInfo, object> dict)
        {
            if (GetValuesMethod == null)
            {
                Task task = Task.Run(() =>
                {
                    foreach (var column in Columns)
                    {
                        var data = column.Value.PropertyInfo.GetValue(obj);
                
                        if (data != null && !column.Value.IsAutoIncrement && !column.Value.IsForeignKey)
                            dict.Add(column.Value, data);
                    }
                });

                CompileGetValuesMethod(type);
                task.Wait();
            }
            else
                GetValuesMethod.DynamicInvoke(obj, dict);
        }

        public Dictionary<string, Tuple<Tuple<int, PropertyInfo>, CryEntityColumnInfo>> GetColumnsWithAnonType(Type type)
        {
            var result = new Dictionary<string, Tuple<Tuple<int, PropertyInfo>, CryEntityColumnInfo>>();
            var properties = type.GetProperties();
            int index = 0;

            foreach (var prop in properties)
            {
                var column = FindColumn(prop.Name);

                if (column != null)
                    result.Add(column.Alias, new Tuple<Tuple<int, PropertyInfo>, CryEntityColumnInfo>(new Tuple<int, PropertyInfo>(index++, prop), column));
            }

            return result;
        }

        public CryEntityColumnInfo GetColumn(PropertyInfo propertyInfo)
        {
            if (PrimaryKey.PropertyInfo != null)
            {
                if (PrimaryKey.PropertyInfo.Name == propertyInfo.Name)
                    return PrimaryKey;
            }

            foreach (var column in Columns)
            {
                if (column.Value.PropertyInfo.Name == propertyInfo.Name)
                    return column.Value;
            }

            foreach (var column in ForeignKeys)
            {
                if (column.ForeignKey.PropertyInfo != null)
                {
                    if (column.ForeignKey.PropertyInfo.Name == propertyInfo.Name)
                        return column.ForeignKey;
                }
            }

            return null;
        }

        public CryEntityColumnInfo GetColumn(string strName)
        {
            if (PrimaryKey.PropertyInfo != null)
            {
                if (PrimaryKey.PropertyInfo.Name == strName)
                    return PrimaryKey;
            }

            foreach (var column in Columns)
            {
                if (column.Value.PropertyInfo.Name == strName)
                    return column.Value;
            }

            foreach (var column in ForeignKeys)
            {
                if (column.ForeignKey.PropertyInfo != null)
                {
                    if (column.ForeignKey.PropertyInfo.Name == strName)
                        return column.ForeignKey;
                }
            }

            return null;
        }

        public CryEntityColumnInfo FindColumn(string strName)
        {
            var cur = this;

            while (cur != null)
            {
                CryEntityColumnInfo column;

                foreach (var refTable in ReferencedTables)
                {
                    column = refTable.TableInfo.FindColumn(strName);

                    if (column != null)
                        return column;
                }

                column = cur.GetColumn(strName);

                if (column != null)
                    return column;

                cur = cur.Base;
            }

            return null;
        }

        private void CompileFillMethod<T>()
        {
            var breakLabel = Expression.Label("breakLabel");
            var myObjVar = Expression.Variable(Type, "myObj");
            var keyValuePairType = typeof(KeyValuePair<,>).MakeGenericType(Type, typeof(Dictionary<string, Dictionary<object, object>>));
            var keyValuePairVar = Expression.Variable(keyValuePairType, "keyValuePair");
            var keyValuePairCtor = keyValuePairType.GetConstructor(new[] { Type, typeof(Dictionary<string, Dictionary<object, object>>) });
            var obj2Var = Expression.Variable(keyValuePairType, "obj2");
            var tmpVar = Expression.Variable(typeof(object), "tmp");
            var dest = Expression.Parameter(typeof(ICollection<T>), "dest");
            var dictVar = Expression.Parameter(typeof(Dictionary<,>).MakeGenericType(typeof(object), keyValuePairType), "dict");
            var dictTryGetValue = dictVar.Type.GetMethod("TryGetValue");
            var dictAdd = dictVar.Type.GetMethod("Add");
            var collectionAdd = typeof(ICollection<T>).GetMethod("Add");
            var allowCopyVar = Expression.Parameter(typeof(bool), "allowCopy");

            var loopExpressions = new List<Expression>();
            var FillFullObjectExpressions = new List<Expression>();
            var FillForeignKeysExpressions = new List<Expression>
            {
                Expression.Assign(keyValuePairVar, obj2Var),
                Expression.Assign(myObjVar, Expression.Property(keyValuePairVar, "Key"))
            };

            BuildFillFullObjectExpressions(this, ms_dbReader, myObjVar, tmpVar, keyValuePairVar, FillFullObjectExpressions, true);
            BuildFillForeignKeysExpressions(this, ms_dbReader, myObjVar, tmpVar, keyValuePairVar, FillForeignKeysExpressions);

            loopExpressions.Add(Expression.Assign(tmpVar, Expression.Property(ms_dbReader, "Item", Expression.Constant(Name + PrimaryKey.ColumnName))));
            loopExpressions.Add(Expression.Block(new[] { obj2Var },
                // try to find object otherwise create it
                Expression.IfThenElse(allowCopyVar, 
                            Expression.Block(Expression.Assign(keyValuePairVar,
                                Expression.New(keyValuePairCtor,
                                    Expression.New(Type), Expression.New(typeof(Dictionary<string, Dictionary<object, object>>)))),
                                Expression.Call(dest, collectionAdd, Expression.Property(keyValuePairVar, "Key")),
                                Expression.Assign(myObjVar, Expression.Property(keyValuePairVar, "Key")),
                                Expression.Assign(
                                            Expression.Property(Expression.Property(keyValuePairVar, "Key"), PrimaryKey.PropertyInfo),
                                            Expression.Convert(tmpVar, PrimaryKey.PropertyInfo.PropertyType)),
                                Expression.Block(FillFullObjectExpressions)), 
                            Expression.IfThenElse(
                                Expression.Not(Expression.Call(dictVar, dictTryGetValue, tmpVar, obj2Var)),
                                    Expression.Block(
                                        Expression.Assign(keyValuePairVar, 
                                            Expression.New(keyValuePairCtor, 
                                                Expression.New(Type), Expression.New(typeof(Dictionary<string, Dictionary<object, object>>)))),
                                        Expression.Call(dictVar, dictAdd, tmpVar, keyValuePairVar), // TODO: NEED FIX FOR UNION ALL
                                        Expression.Call(dest, collectionAdd, Expression.Property(keyValuePairVar, "Key")),
                                        Expression.Assign(myObjVar, Expression.Property(keyValuePairVar, "Key")),
                                        Expression.Assign(
                                            Expression.Property(Expression.Property(keyValuePairVar, "Key"), PrimaryKey.PropertyInfo), 
                                            Expression.Convert(tmpVar, PrimaryKey.PropertyInfo.PropertyType)),
                                        Expression.Block(FillFullObjectExpressions)),
                                    Expression.Block(FillForeignKeysExpressions)))));

            var loop = Expression.Loop(
                Expression.IfThenElse(
                       Expression.Call(ms_dbReader, ms_DbReaderRead),
                                Expression.Block(new[] { myObjVar, keyValuePairVar, tmpVar }, loopExpressions),
                                Expression.Break(breakLabel)), breakLabel);

            var lambda = Expression.Lambda<FillObjectDelegate<T>>(loop, ms_dbReader, dest, allowCopyVar, dictVar);

            FillMethod = lambda.Compile();
        }
        
        private void FillObject<T>(CryEntityTableInfo tableInfo, DbDataReader reader, 
            KeyValuePair<T, Dictionary<string, Dictionary<object, object>>> obj, 
            object dest, bool isFirstStart = false)
        {
            // Reflection version to load complete database structure into objects.
            // slow version!
            // will be used until the fast method is compiled.

            var currTable = tableInfo;

            while (currTable != null)
            {
                foreach (var column in currTable.ForeignKeys)
                {
                    if (column.PrimaryKey.TableInfo == currTable.Base)
                        continue;

                    if (column.IsCollection)
                    {
                        // collection -> try to find collection and add new elements
                        var tmp = reader[column.PrimaryKey.Alias];

                        if (tmp is not DBNull)
                        {
                            if (!obj.Value.TryGetValue(column.ForeignKey.ColumnName, out var currDict))
                            {
                                // collection not found -> add new collection and new element
                                var myObj = Activator.CreateInstance(column.CollectionGenericTypes[0]);
                                IList lst = (IList)Activator.CreateInstance(column.CollectionType);
                                lst.Add(myObj);
                                FillObject(column.PrimaryKey.TableInfo, reader, obj, myObj);
                                obj.Value.Add(column.ForeignKey.ColumnName, new Dictionary<object, object>() { { tmp, myObj } });
                                column.ForeignKey.PropertyInfo.SetValue(dest, lst);
                            }
                            else if (!currDict.ContainsKey(tmp))
                            {
                                // collection found -> add new element
                                var lst = (IList)column.ForeignKey.PropertyInfo.GetValue(dest);
                                var myObj = Activator.CreateInstance(column.CollectionGenericTypes[0]);
                                lst.Add(myObj);
                                FillObject(column.PrimaryKey.TableInfo, reader, obj, myObj);
                                currDict.Add(tmp, myObj);
                            }
                        }
                    }
                    else if (column.IsDictionary)
                    {
                        // dictionary -> try to find collection and add new elements
                        var tmp = reader[column.PrimaryKey.Alias];

                        if (tmp is not DBNull)
                        {
                            if (!obj.Value.TryGetValue(column.ForeignKey.ColumnName, out var currDict))
                            {
                                // dictionary not found -> add new dictionary and new element
                                var myObj = Activator.CreateInstance(column.CollectionGenericTypes[1]);
                                IDictionary lst = (IDictionary)Activator.CreateInstance(column.CollectionType);
                                lst.Add(tmp, myObj);
                                FillObject(column.PrimaryKey.TableInfo, reader, obj, myObj);
                                obj.Value.Add(column.ForeignKey.ColumnName, new Dictionary<object, object>() { { tmp, myObj } });
                                column.ForeignKey.PropertyInfo.SetValue(dest, lst);
                            }
                            else if (!currDict.ContainsKey(tmp))
                            {
                                // dictionary found -> add new element
                                var lst = (IDictionary)column.ForeignKey.PropertyInfo.GetValue(dest);
                                var myObj = Activator.CreateInstance(column.CollectionGenericTypes[1]);
                                lst.Add(tmp, myObj);
                                FillObject(column.PrimaryKey.TableInfo, reader, obj, myObj);
                                currDict.Add(tmp, myObj);
                            }
                        }   
                    }
                    else
                    {
                        // no dictionary or collection set the value direct to property
                        var myObj = Activator.CreateInstance(column.ForeignKey.PropertyInfo.PropertyType);
                        FillObject(column.PrimaryKey.TableInfo, reader, obj, myObj);
                        column.ForeignKey.PropertyInfo.SetValue(dest, myObj);
                    }
                }

                // set all 'normal' properties
                foreach (var column in currTable.Columns)
                {
                    if (!column.Value.IsPrimaryKey || !isFirstStart)
                    {
                        var tmp = reader[column.Value.Alias];

                        if (tmp is not DBNull)
                            column.Value.PropertyInfo.SetValue(dest, tmp);
                    }
                }

                // move to inherited type
                currTable = currTable.Base;
                isFirstStart = false;
            }
        }

        private void CompileGetValuesMethod(Type type)
        {
            var tmpExpressions = new List<Expression>();
            var src = Expression.Parameter(Type, "src");

            foreach (var c in Columns)
            {
                if (c.Value.IsAutoIncrement)
                    continue;

                tmpExpressions.Add(Expression.Call(ms_dict, ms_dictAdd,
                    Expression.Constant(c.Value),
                    Expression.Convert(
                        Expression.Property(src, c.Value.PropertyInfo.Name), typeof(object))));
            }

            var lambda = Expression.Lambda(typeof(GetValuesDelegate<>).MakeGenericType(type), Expression.Block(tmpExpressions), src, ms_dict);

            GetValuesMethod = lambda.Compile();
        }

        private void BuildFillFullObjectExpressions(CryEntityTableInfo table, Expression reader, Expression dest, Expression tmp, Expression keyValuePair, List<Expression> output, bool isFirstStart = false)
        {
            var currTable = table;

            while (currTable != null)
            {
                BuildFillForeignKeysExpression(currTable, reader, dest, tmp, keyValuePair, output);

                // set all 'normal' properties
                foreach (var column in currTable.Columns)
                {
                    if (column.Value.IsPrimaryKey && isFirstStart)
                        continue;

                    if (column.Value.IsNotNull)
                    {
                        output.Add(Expression.Assign(
                            Expression.Property(dest, column.Value.PropertyInfo), 
                            Expression.Convert(
                                Expression.Property(ms_dbReader, "Item", 
                                    Expression.Constant(column.Value.Alias)), column.Value.PropertyInfo.PropertyType)));
                    }
                    else
                    {
                        output.Add(Expression.Assign(tmp, Expression.Property(ms_dbReader, "Item", Expression.Constant(column.Value.Alias))));

                        output.Add(Expression.IfThenElse(Expression.TypeIs(tmp, typeof(DBNull)), 
                            Expression.Assign(
                                Expression.Property(dest, column.Value.PropertyInfo), Expression.Default(column.Value.PropertyInfo.PropertyType)),
                            Expression.Assign(
                                Expression.Property(dest, column.Value.PropertyInfo), Expression.Convert(tmp, column.Value.PropertyInfo.PropertyType))));
                    }
                }

                // move to inherited type
                currTable = currTable.Base;
                isFirstStart = false;
            }
        }

        private void BuildFillForeignKeysExpressions(CryEntityTableInfo table, Expression reader, Expression dest, Expression tmp, Expression keyValuePair, List<Expression> output)
        {
            var currTable = table;

            while (currTable != null)
            {
                BuildFillForeignKeysExpression(currTable, reader, dest, tmp, keyValuePair, output);
                currTable = currTable.Base;
            }
        }

        private void BuildFillForeignKeysExpression(CryEntityTableInfo table, Expression reader, Expression dest, Expression tmp, Expression keyValuePair, List<Expression> output)
        {
            foreach (var foreign in table.ForeignKeys)
            {
                if (foreign.PrimaryKey.TableInfo == table.Base)
                    continue;

                if (foreign.IsCollection)
                {
                    // collection -> try to find collection and add new elements
                    output.Add(Expression.Assign(tmp, Expression.Property(ms_dbReader, "Item", Expression.Constant(foreign.PrimaryKey.Alias))));

                    var currDictVar = Expression.Variable(typeof(Dictionary<object, object>), "currDict");
                    var myDestVar = Expression.Variable(foreign.CollectionGenericTypes[0], "tmpDest");
                    var listVar = Expression.Variable(foreign.CollectionType, "list");


                    var expressions = new List<Expression>();

                    BuildFillFullObjectExpressions(foreign.PrimaryKey.TableInfo, reader, myDestVar, tmp, keyValuePair, expressions);

                    var listAdd = listVar.Type.GetMethod("Add");
                    var realListAdd = foreign.RealCollectionType.GetMethod("Add");

                    var expression = Expression.Block(new [] { currDictVar },
                        Expression.IfThenElse(
                            Expression.Not(Expression.Call(Expression.Property(keyValuePair, "Value"), ms_columnDictTryGetValue, Expression.Constant(foreign.ForeignKey.ColumnName), currDictVar)),
                                // collection not found -> add new collection and new element
                                Expression.Block(new[] { myDestVar, listVar },
                                    Expression.Assign(myDestVar, Expression.New(foreign.CollectionGenericTypes[0])),
                                    Expression.Assign(listVar, Expression.New(foreign.CollectionType)),
                                    Expression.Call(listVar, listAdd, myDestVar),
                                    Expression.Block(expressions),
                                    Expression.Assign(currDictVar, Expression.New(typeof(Dictionary<object, object>))),
                                    Expression.Call(Expression.Property(keyValuePair, "Value"), ms_columnDictAdd, Expression.Constant(foreign.ForeignKey.ColumnName), currDictVar),
                                    Expression.Call(currDictVar, ms_objectDictAdd, tmp, myDestVar),
                                    Expression.Assign(Expression.Property(dest, foreign.ForeignKey.PropertyInfo), listVar)), 
                            Expression.IfThen(Expression.Not(Expression.Call(currDictVar, ms_objectDictAddContainsKey, tmp)),
                                // collection found -> add new element
                                Expression.Block(new[] { myDestVar, listVar },
                                    Expression.Assign(myDestVar, Expression.New(foreign.CollectionGenericTypes[0])),
                                    Expression.Call(Expression.Property(dest, foreign.ForeignKey.PropertyInfo), realListAdd, myDestVar),
                                    Expression.Block(expressions),
                                    Expression.Call(currDictVar, ms_objectDictAdd, tmp, myDestVar)))));

                    if (foreign.ForeignKey.IsNotNull)
                        output.Add(expression);
                    else
                        output.Add(Expression.IfThen(Expression.Not(Expression.TypeIs(tmp, typeof(DBNull))), expression));
                }
                else if (foreign.IsDictionary)
                {
                    // dictionary -> try to find collection and add new elements

                    output.Add(Expression.Assign(tmp, Expression.Property(ms_dbReader, "Item", Expression.Constant(foreign.PrimaryKey.Alias))));

                    var currDictVar = Expression.Variable(typeof(Dictionary<object, object>), "currDict");
                    var myDestVar = Expression.Variable(foreign.CollectionGenericTypes[1], "tmpDest");
                    var dictVar = Expression.Variable(foreign.CollectionType, "list");

                    var listAdd = dictVar.Type.GetMethod("Add");
                    var realListAdd = foreign.RealCollectionType.GetMethod("Add");

                    var expressions = new List<Expression>();
                    BuildFillFullObjectExpressions(foreign.PrimaryKey.TableInfo, reader, myDestVar, tmp, keyValuePair, expressions);

                    var expression = Expression.Block(new[] { currDictVar },
                        Expression.IfThenElse(
                            Expression.Not(Expression.Call(Expression.Property(keyValuePair, "Value"), ms_columnDictTryGetValue, Expression.Constant(foreign.ForeignKey.ColumnName), currDictVar)),
                                // dictionary not found -> add new dictionary and new element
                                Expression.Block(new[] { myDestVar, dictVar },
                                    Expression.Assign(myDestVar, Expression.New(foreign.CollectionGenericTypes[1])),
                                    Expression.Assign(dictVar, Expression.New(foreign.CollectionType)),
                                    Expression.Call(dictVar, listAdd, Expression.Convert(tmp, foreign.CollectionGenericTypes[0]), myDestVar),
                                    Expression.Block(expressions),
                                    Expression.Assign(currDictVar, Expression.New(typeof(Dictionary<object, object>))),
                                    Expression.Call(Expression.Property(keyValuePair, "Value"), ms_columnDictAdd, Expression.Constant(foreign.ForeignKey.ColumnName), currDictVar),
                                    Expression.Call(currDictVar, ms_objectDictAdd, tmp, myDestVar),
                                    Expression.Assign(Expression.Property(dest, foreign.ForeignKey.PropertyInfo), dictVar)),
                            Expression.IfThen(Expression.Not(Expression.Call(currDictVar, ms_objectDictAddContainsKey, tmp)),
                                // dictionary found -> add new element
                                Expression.Block(new[] { myDestVar, dictVar },
                                    Expression.Assign(myDestVar, Expression.New(foreign.CollectionGenericTypes[1])),
                                    Expression.Call(Expression.Property(dest, foreign.ForeignKey.PropertyInfo), realListAdd, Expression.Convert(tmp, foreign.CollectionGenericTypes[0]), myDestVar),
                                    Expression.Block(expressions),
                                    Expression.Call(currDictVar, ms_objectDictAdd, tmp, myDestVar)))));

                    if (foreign.ForeignKey.IsNotNull)
                        output.Add(expression);
                    else
                        output.Add(Expression.IfThen(Expression.Not(Expression.TypeIs(tmp, typeof(DBNull))), expression));
                }
                else
                {
                    // no dictionary or collection set the value direct to property

                    var myDestVar = Expression.Variable(foreign.ForeignKey.PropertyInfo.PropertyType, "tmpDest");

                    var expressions = new List<Expression>
                    {
                        Expression.Assign(myDestVar, Expression.New(foreign.ForeignKey.PropertyInfo.PropertyType))
                    };

                    BuildFillFullObjectExpressions(foreign.PrimaryKey.TableInfo, reader, myDestVar, tmp, keyValuePair, expressions);

                    expressions.Add(Expression.Assign(Expression.Property(dest, foreign.ForeignKey.PropertyInfo), myDestVar));

                    output.Add(Expression.Block(new[] { myDestVar }, expressions));
                }
            }
        }
    }
}
