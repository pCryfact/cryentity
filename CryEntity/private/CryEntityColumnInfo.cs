﻿using System;
using System.Reflection;

namespace CryEntity
{
    class CryEntityColumnInfo
    {
        public PropertyInfo PropertyInfo { get; private set; }
        public CryEntityTableInfo TableInfo { get; }

        public string ColumnName { get; }

        public string Alias { get; set; }

        public bool IsPrimaryKey { get; }
        public bool IsForeignKey { get; }
        public bool IsIgnored { get; private set; }
        public bool IsAutoIncrement { get; private set; }
        public bool IsNotNull { get; private set; }

        public bool OrderBy { get; private set; }
        public CryEntityOrderDirection OrderDirection { get; private set; }

        public CryEntityForeignKeyAttribute ForeignKeyAttribute { get; }

        public CryEntityColumnInfo(CryEntityTableInfo tableInfo, PropertyInfo propertyInfo)
        {
            TableInfo = tableInfo;
            PropertyInfo = propertyInfo;

            var columnNameAttr = (CryEntityColumnNameAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityColumnNameAttribute));

            if (columnNameAttr == null)
                ColumnName = propertyInfo.Name;
            else
                ColumnName = columnNameAttr.Name;

            var columnNameAsAttr = (CryEntityColumnNameAsAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityColumnNameAsAttribute));

            if (columnNameAsAttr == null)
                Alias = tableInfo.Name + ColumnName;
            else
                Alias = columnNameAttr.Name;

            ForeignKeyAttribute = (CryEntityForeignKeyAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityForeignKeyAttribute));

            IsPrimaryKey = (CryEntityPrimaryKeyAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityPrimaryKeyAttribute)) != null;
            IsForeignKey = ForeignKeyAttribute != null;
            IsIgnored = (CryEntityIgnoreAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityIgnoreAttribute)) != null;
            IsAutoIncrement = (CryEntityAutoIncrementAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityAutoIncrementAttribute)) != null;
            IsNotNull = (CryEntityNotNullAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityNotNullAttribute)) != null || IsPrimaryKey;

            var orderByAttr = (CryEntityOrderByAttribute)Attribute.GetCustomAttribute(propertyInfo, typeof(CryEntityOrderByAttribute));

            if (orderByAttr != null)
            {
                OrderBy = true;
                OrderDirection = orderByAttr.OrderDirection;
            }

            if (ForeignKeyAttribute != null)
                ColumnName = ForeignKeyAttribute.ColumnName;
        }

        public CryEntityColumnInfo(CryEntityTableInfo tableInfo, CryEntityForeignKeyAttribute attr)
        {
            TableInfo = tableInfo;
            IsPrimaryKey = false;
            IsForeignKey = true;
            IsIgnored = false;
            ForeignKeyAttribute = attr;
            IsAutoIncrement = false;
            ColumnName = attr.ColumnName;
            Alias = tableInfo.Name + ColumnName;
        }

        public void CopyFrom(CryEntityColumnInfo otherColumn)
        {
            PropertyInfo = otherColumn.PropertyInfo;
            Alias = otherColumn.Alias;
            IsNotNull = otherColumn.IsNotNull;
            IsAutoIncrement = otherColumn.IsAutoIncrement;
            IsIgnored = otherColumn.IsIgnored;
            OrderBy = otherColumn.OrderBy;
            OrderDirection = otherColumn.OrderDirection;
        }
    }
}
