﻿using System;

namespace CryEntity
{
    class CryEntityForeignKeyInfo
    {
        public CryEntityColumnInfo PrimaryKey { get; set; }
        public CryEntityColumnInfo ForeignKey { get; set; }
        public CryEntityJoinType JoinType { get; set; }
        public bool IsCollection { get; set; }
        public bool IsDictionary { get; set; }
        public Type CollectionType { get; set; }
        public Type RealCollectionType { get; set; }
        public Type[] CollectionGenericTypes { get; set; }

        public CryEntityForeignKeyInfo(CryEntityColumnInfo foreignKey, CryEntityColumnInfo primaryKey)
        {
            ForeignKey = foreignKey;
            JoinType = foreignKey.ForeignKeyAttribute.JoinType;
            PrimaryKey = primaryKey;
        }
    }
}
