﻿using System.Data.Common;

namespace CryEntity
{
    public interface ICryEntityDataBase
    {
        DbConnection CreateConnection();
    }
}
