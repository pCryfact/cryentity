﻿using System;
using System.Text;
using System.Data.Common;

namespace CryEntity
{
    public class CryEntityMySqlQueryBuilder : ICryEntityQueryBuilder
    {
        public virtual string CreateSelectQuery(CryEntityQueryStatement createQueryInfo)
        {
            if (createQueryInfo.IsUnion)
                return CreateUnionQuery(createQueryInfo, createQueryInfo.Next, CryEntityQueryType.Select, false);
            else if (createQueryInfo.IsUnionAll)
                return CreateUnionQuery(createQueryInfo, createQueryInfo.Next, CryEntityQueryType.Select, true);
            else if (createQueryInfo.IsExcept)
                return CreateExceptQuery(createQueryInfo, createQueryInfo.Next, CryEntityQueryType.Select);

            return CreateSelectQuery(createQueryInfo.QueryInfo);
        }

        public virtual string CreateInsertQuery(CryEntityQueryStatement createQueryInfo)
        {
            string strColumnNames = GetColumnNames(createQueryInfo.QueryInfo);

            if (string.IsNullOrEmpty(strColumnNames))
                return string.Empty;

            return $"INSERT INTO {createQueryInfo.QueryInfo.TableName} ({strColumnNames}) VALUES({GetParameters(createQueryInfo.QueryInfo)})";
        }

        public virtual string CreateUpdateQuery(CryEntityQueryStatement createQueryInfo)
        {
            string strSets = GetColumnParameterAssigns(createQueryInfo.QueryInfo);

            if (string.IsNullOrEmpty(strSets))
                return string.Empty;

            if (createQueryInfo.QueryInfo.Where == null)
                return $"UPDATE {createQueryInfo.QueryInfo.TableName} SET {strSets}";
            else
            {
                int idx = 0;

                return $"UPDATE {createQueryInfo.QueryInfo.TableName} SET {strSets} WHERE {CreateWhere(createQueryInfo.QueryInfo.Where, createQueryInfo.QueryInfo.Command, "where", ref idx)}";
            }
        }

        public virtual string CreateGetLastInsertedIdQuery()
        {
            return $"SELECT LAST_INSERT_ID()";
        }

        public virtual string CreateDeleteQuery(CryEntityQueryStatement createQueryInfo)
        {
            if (createQueryInfo.QueryInfo.Where == null)
                return $"TRUNCATE TABLE {createQueryInfo.QueryInfo.TableName}";
            else
            {
                int idx = 0;

                return $"DELETE FROM {createQueryInfo.QueryInfo.TableName} WHERE {CreateWhere(createQueryInfo.QueryInfo.Where, createQueryInfo.QueryInfo.Command, "where", ref idx)}";
            }
        }

        protected virtual string CreateUnionQuery(CryEntityQueryStatement statement1, CryEntityQueryStatement statement2, CryEntityQueryType queryType, bool isAll)
        {
            switch (queryType)
            {
                case CryEntityQueryType.Select:
                    {
                        string query1 = CreateSelectQuery(statement1.QueryInfo);
                        string query2 = CreateSelectQuery(statement2.QueryInfo);

                        if (isAll)
                            return query1 + " UNION ALL " + query2;

                        return query1 + " UNION " + query2;
                    }
            }

            throw new NotSupportedException();
        }

        protected virtual string GetPrimaryKeyAliasName(CryEntityCreateQueryInfo createQueryInfo)
        {
            var primaryKey = createQueryInfo.GetPrimaryKey();

            if (string.IsNullOrEmpty(primaryKey.Alias))
                return $"{primaryKey.TableName}{primaryKey.Name}";

            return primaryKey.Alias;
        }

        protected virtual string GetPrimaryKeyName(CryEntityCreateQueryInfo createQueryInfo)
        {
            var primaryKey = createQueryInfo.GetPrimaryKey();

            return primaryKey.Name;
        }

        protected virtual string CreateExceptQuery(CryEntityQueryStatement statement1, CryEntityQueryStatement statement2, CryEntityQueryType queryType)
        {
            // mysql does not support except, using workaround with left join

            switch (queryType)
            {
                case CryEntityQueryType.Select:
                    {
                        string query2 = CreateSelectQuery(statement2.QueryInfo);

                        string query1PrimaryKey = $"{statement1.QueryInfo.TableName}." + GetPrimaryKeyName(statement1.QueryInfo);
                        string query2PrimaryKey = "tab2." + GetPrimaryKeyAliasName(statement2.QueryInfo);

                        string strJoin = $"LEFT JOIN ({query2}) AS tab2 ON {query1PrimaryKey} = {query2PrimaryKey}";

                        if (statement1.QueryInfo.Joins != null && statement1.QueryInfo.Joins.Count > 0)
                            strJoin = CreateJoin(statement1.QueryInfo);

                        string strWhere;

                        if (statement1.QueryInfo.Where != null)
                        {
                            int idx = statement1.QueryInfo.Command.Parameters.Count;
                            strWhere = $"{query2PrimaryKey} IS NULL AND " + CreateWhere(statement1.QueryInfo.Where, statement1.QueryInfo.Command, "where", ref idx);
                        }
                        else
                            strWhere = $"{query2PrimaryKey} IS NULL";

                        return CreateSelectQuery(statement1.QueryInfo, strJoin, strWhere);
                    }
            }

            throw new NotSupportedException();
        }

        protected virtual string CreateSelectQuery(CryEntityCreateQueryInfo createQueryInfo)
        {
            string strJoin = string.Empty;

            if (createQueryInfo.Joins != null && createQueryInfo.Joins.Count > 0)
                strJoin = " " + CreateJoin(createQueryInfo);

            string strWhere = string.Empty;

            if (createQueryInfo.Where != null)
            {
                int idx = createQueryInfo.Command.Parameters.Count;
                strWhere = CreateWhere(createQueryInfo.Where, createQueryInfo.Command, "where", ref idx);
            }

            return CreateSelectQuery(createQueryInfo, strJoin, strWhere);
        }

        protected virtual string CreateSelectQuery(CryEntityCreateQueryInfo createQueryInfo, string strJoin, string strWhere)
        {
            string strOrderBy = string.Empty;
            string strQuery;

            if (createQueryInfo.OrderBy != null)
                strOrderBy = " " + CreateOrderBy(createQueryInfo);

            if (createQueryInfo.GroupBy != null)
                strOrderBy += " " + CreateGroupBy(createQueryInfo);

            string strColumns = GetColumnWithAliasNames(createQueryInfo, out var hasAggregate);

            if (string.IsNullOrEmpty(strColumns))
            {
                string strAggregate = GetAggregateFunction(createQueryInfo.Manipulator);

                if (string.IsNullOrEmpty(strAggregate))
                    strColumns = "*";
                else
                    strColumns = $"{strAggregate}(*)";
            }
            else
            {
                string strAggregate = GetAggregateFunction(createQueryInfo.Manipulator);

                if (createQueryInfo.Manipulator == CryEntityColumnManipulator.Count && !string.IsNullOrEmpty(strAggregate))
                    strColumns = $"{strAggregate}(*)";
            }

            if (createQueryInfo.Manipulator != CryEntityColumnManipulator.None)
                hasAggregate = true;

            if (string.IsNullOrEmpty(strWhere))
            {
                if (createQueryInfo.Limit != 0)
                {
                    if (createQueryInfo.Offset != 0)
                    {
                        if (hasAggregate || !string.IsNullOrEmpty(strOrderBy))
                        {
                            string strSubQuery = $"SELECT * FROM {createQueryInfo.TableName} LIMIT {createQueryInfo.Offset},{createQueryInfo.Limit}";

                            strColumns = strColumns.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                            strJoin = strJoin.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                            strOrderBy = strOrderBy.Replace(createQueryInfo.TableName + ".", "subQueryTable.");

                            strQuery = $"SELECT {strColumns} FROM ({strSubQuery}) AS subQueryTable{strJoin}{strOrderBy}";
                        }
                        else
                            strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName}{strJoin}{strOrderBy} LIMIT {createQueryInfo.Offset},{createQueryInfo.Limit}";
                    }
                    else
                    {
                        if (hasAggregate || !string.IsNullOrEmpty(strOrderBy))
                        {
                            string strSubQuery = $"SELECT * FROM {createQueryInfo.TableName} LIMIT {createQueryInfo.Limit}";

                            strColumns = strColumns.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                            strJoin = strJoin.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                            strOrderBy = strOrderBy.Replace(createQueryInfo.TableName + ".", "subQueryTable.");

                            strQuery = $"SELECT {strColumns} FROM ({strSubQuery}) AS subQueryTable{strJoin}{strOrderBy}";
                        }
                        else
                            strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName}{strJoin}{strOrderBy} LIMIT {createQueryInfo.Limit}";
                    }
                }
                else if (createQueryInfo.Offset != 0)
                {
                    if (hasAggregate || !string.IsNullOrEmpty(strOrderBy))
                    {
                        string strSubQuery = $"SELECT * FROM {createQueryInfo.TableName} LIMIT {createQueryInfo.Offset},1";

                        strColumns = strColumns.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strJoin = strJoin.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strOrderBy = strOrderBy.Replace(createQueryInfo.TableName + ".", "subQueryTable.");

                        strQuery = $"SELECT {strColumns} FROM ({strSubQuery}) AS subQueryTable{strJoin}{strOrderBy}";
                    }
                    else
                        strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName}{strJoin}{strOrderBy} LIMIT {createQueryInfo.Offset},1";
                }
                else
                    strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName} {strJoin}{strOrderBy}";
            }
            else if (createQueryInfo.Limit != 0)
            {
                if (createQueryInfo.Offset != 0)
                {
                    if (hasAggregate || !string.IsNullOrEmpty(strOrderBy))
                    {
                        string strSubQuery = $"SELECT * FROM {createQueryInfo.TableName} LIMIT {createQueryInfo.Offset},{createQueryInfo.Limit}";

                        strColumns = strColumns.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strJoin = strJoin.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strWhere = strWhere.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strOrderBy = strOrderBy.Replace(createQueryInfo.TableName + ".", "subQueryTable.");

                        strQuery = $"SELECT {strColumns} FROM ({strSubQuery}) AS subQueryTable {strJoin} WHERE {strWhere}{strOrderBy}";
                    }
                    else
                        strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName} {strJoin} WHERE {strWhere}{strOrderBy} LIMIT {createQueryInfo.Offset},{createQueryInfo.Limit}";
                }
                else
                {
                    if (hasAggregate || !string.IsNullOrEmpty(strOrderBy))
                    {
                        string strSubQuery = $"SELECT * FROM {createQueryInfo.TableName} LIMIT {createQueryInfo.Limit}";

                        strColumns = strColumns.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strJoin = strJoin.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strWhere = strWhere.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                        strOrderBy = strOrderBy.Replace(createQueryInfo.TableName + ".", "subQueryTable.");

                        strQuery = $"SELECT {strColumns} FROM ({strSubQuery}) AS subQueryTable {strJoin} WHERE {strWhere}{strOrderBy}";
                    }
                    else
                        strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName} {strJoin} WHERE {strWhere}{strOrderBy} LIMIT {createQueryInfo.Limit}";
                }
            }
            else if (createQueryInfo.Offset != 0)
            {
                if (hasAggregate || !string.IsNullOrEmpty(strOrderBy))
                {
                    string strSubQuery = $"SELECT * FROM {createQueryInfo.TableName} LIMIT {createQueryInfo.Offset},1";

                    strColumns = strColumns.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                    strJoin = strJoin.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                    strWhere = strWhere.Replace(createQueryInfo.TableName + ".", "subQueryTable.");
                    strOrderBy = strOrderBy.Replace(createQueryInfo.TableName + ".", "subQueryTable.");

                    strQuery = $"SELECT {strColumns} FROM ({strSubQuery}) AS subQueryTable {strJoin} WHERE {strWhere}{strOrderBy}";
                }
                else
                    strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName} {strJoin} WHERE {strWhere}{strOrderBy} LIMIT {createQueryInfo.Offset},1";
            }
            else
                strQuery = $"SELECT {strColumns} FROM {createQueryInfo.TableName} {strJoin} WHERE {strWhere}{strOrderBy}";

            return strQuery;
        }

        protected virtual string CreateWhere(CryEntityQueryWhereOnInfo info, DbCommand cmd, string strVarName, ref int idx)
        {
            var currWhere = info;
            var sb = new StringBuilder();

            while (currWhere != null)
            {
                var strWhere = GetSingleWhere(currWhere, cmd, strVarName, ref idx);
                sb.Append(strWhere);

                if (currWhere.Next != null)
                {
                    switch (currWhere.NextOperator)
                    {
                        case CryEntityQueryLogicalOperator.And:
                            sb.Append(" AND ");
                            break;
                        case CryEntityQueryLogicalOperator.Or:
                            sb.Append(" OR ");
                            break;
                        case CryEntityQueryLogicalOperator.Xor:
                            sb.Append(" XOR ");
                            break;
                    }
                }
                else
                    sb.Append(' ');

                currWhere = currWhere.Next;
            }

            return sb.Remove(sb.Length - 1, 1).ToString();
        }

        protected virtual string GetColumnNames(CryEntityCreateQueryInfo info)
        {
            var sb = new StringBuilder();

            foreach (var column in info.Columns)
                sb.Append($"{column.Name}, ");



            if (sb.Length > 2)
                return sb.Remove(sb.Length - 2, 2).ToString();

            return string.Empty;
        }

        protected virtual string GetAggregateFunction(CryEntityColumnManipulator manipulator)
        {
            return manipulator switch
            {
                CryEntityColumnManipulator.None => string.Empty,
                CryEntityColumnManipulator.Average => "AVG",
                CryEntityColumnManipulator.Min => "MIN",
                CryEntityColumnManipulator.Max => "MAX",
                CryEntityColumnManipulator.Count => "COUNT",
                CryEntityColumnManipulator.Sum => "SUM",
                _ => throw new ArgumentException("invalid manipulator", nameof(manipulator)),
            };
        }

        protected virtual string GetColumnWithAliasName(CryEntityCreateQueryColumnInfo columnInfo, ref bool hasAggregate, ref int unkIdx)
        {
            string strAggregate = GetAggregateFunction(columnInfo.Manipulator);

            if (string.IsNullOrEmpty(strAggregate))
                return $"{columnInfo.TableName}.{columnInfo.Name} AS {columnInfo.Alias}";
            else
            {
                hasAggregate = true;

                if (string.IsNullOrEmpty(columnInfo.Name) && string.IsNullOrEmpty(columnInfo.TableName))
                    return $"{strAggregate}(*) AS {columnInfo.Alias}";
                else
                    return $"{strAggregate}({columnInfo.TableName}.{columnInfo.Name}) AS {columnInfo.Alias}";
            }
        }

        protected virtual string GetColumnWithAliasNames(CryEntityCreateQueryInfo info, out bool hasAggregate)
        {
            var sb = new StringBuilder();
            hasAggregate = false;
            int unkIdx = 0;

            foreach (var column in info.Columns)
                sb.Append(GetColumnWithAliasName(column, ref hasAggregate, ref unkIdx) + ", ");

            return sb.Remove(sb.Length - 2, 2).ToString();
        }

        protected virtual string CreateJoin(CryEntityCreateQueryInfo info)
        {
            var sb = new StringBuilder();

            int idx = 0;

            foreach (var join in info.Joins)
                sb.Append($"{GetJoinType(join.JoinType)} {join.Table} ON {CreateWhere(join.On, info.Command, "join", ref idx)} ");

            return sb.Remove(sb.Length - 1, 1).ToString();
        }

        protected virtual string GetParameters(CryEntityCreateQueryInfo info)
        {
            var sb = new StringBuilder();

            foreach (var column in info.Columns)
                sb.Append($"@{column.Parameter.ParameterName}, ");

            return sb.Remove(sb.Length - 2, 2).ToString();
        }

        protected virtual string GetColumnParameterAssigns(CryEntityCreateQueryInfo info)
        {
            var sb = new StringBuilder();

            foreach (var column in info.Columns)
                sb.Append($"{column.Name} = @{column.Parameter.ParameterName}, ");

            if (sb.Length > 2)
                return sb.Remove(sb.Length - 2, 2).ToString();

            return string.Empty;
        }

        protected virtual string GetJoinType(CryEntityJoinType joinType)
        {
            string strJoinType = string.Empty;

            if (joinType == CryEntityJoinType.Default)
                strJoinType = "JOIN";
            else if ((joinType & CryEntityJoinType.Straight) != 0)
                strJoinType = "STRAIGHT_JOIN";
            else
            {
                if (((joinType & CryEntityJoinType.Left) != 0) ||
                    ((joinType & CryEntityJoinType.Right) != 0))
                {
                    if ((joinType & CryEntityJoinType.Left) != 0)
                        strJoinType = "LEFT";
                    else if ((joinType & CryEntityJoinType.Right) != 0)
                        strJoinType = "RIGHT";

                    if ((joinType & CryEntityJoinType.Outer) != 0)
                        strJoinType += " OUTER";

                    strJoinType += " JOIN";
                }
                else if ((joinType & CryEntityJoinType.Inner) != 0)
                {
                    if ((joinType & CryEntityJoinType.Natural) != 0)
                        strJoinType = "NATURAL ";

                    strJoinType += "INNER JOIN";
                }
                else if ((joinType & CryEntityJoinType.Cross) != 0)
                    strJoinType = "CROSS JOIN";
            }

            return strJoinType;
        }

        protected virtual string GetComparison(object left, object right, object optionalParam, string strCmpOperator, DbCommand cmd, string strVarName, CryEntityQueryOperator op, ref int idx)
        {
            switch (op)
            {
                case CryEntityQueryOperator.Between:
                    {
                        string strResult;

                        if (left is CryEntityCreateQueryColumnInfo sec1)
                            strResult = $"{sec1.TableName}.{sec1.Name} {strCmpOperator} ";
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = left;
                            cmd.Parameters.Add(param1);

                            strResult = $"@{param1.ParameterName} {strCmpOperator} ";
                        }

                        if (right is CryEntityCreateQueryColumnInfo sec2)
                            strResult += $"{sec2.TableName}.{sec2.Name} AND ";
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = right;
                            cmd.Parameters.Add(param1);

                            strResult += $"@{param1.ParameterName} AND ";
                        }

                        if (optionalParam is CryEntityCreateQueryColumnInfo sec3)
                            strResult += $"{sec3.TableName}.{sec3.Name} ";
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = optionalParam;
                            cmd.Parameters.Add(param1);

                            strResult += $"@{param1.ParameterName}";
                        }

                        return strResult;
                    }
                case CryEntityQueryOperator.IsNull:
                    {
                        if (left is CryEntityCreateQueryColumnInfo sec)
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = right;
                            cmd.Parameters.Add(param1);
                            return $"{sec.TableName}.{sec.Name} {strCmpOperator}";
                        }
                        else
                        {
                            var param1 = cmd.CreateParameter();

                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = left;

                            cmd.Parameters.Add(param1);

                            return $"@{param1.ParameterName} {strCmpOperator}";
                        }
                    }
                case CryEntityQueryOperator.StringCompare:
                    {
                        string strResult;

                        if (left is CryEntityCreateQueryColumnInfo sec1)
                            strResult = $"{strCmpOperator}({sec1.TableName}.{sec1.Name}, ";
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = left;
                            cmd.Parameters.Add(param1);

                            strResult = $"{strCmpOperator}(@{param1.ParameterName}, ";
                        }

                        if (right is CryEntityCreateQueryColumnInfo sec2)
                            strResult += $"{sec2.TableName}.{sec2.Name})";
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = right;
                            cmd.Parameters.Add(param1);

                            strResult += $"@{param1.ParameterName})";
                        }

                        return strResult;
                    }
                default:
                    {
                        string strResult;

                        if (left is CryEntityCreateQueryColumnInfo sec1)
                        {
                            if (sec1.Manipulator != CryEntityColumnManipulator.None)
                            {
                                if (string.IsNullOrEmpty(sec1.TableName) || string.IsNullOrEmpty(sec1.Name))
                                    strResult = $"{GetAggregateFunction(sec1.Manipulator)}(*) {strCmpOperator} ";
                                else
                                    strResult = $"{GetAggregateFunction(sec1.Manipulator)}({sec1.TableName}.{sec1.Name}) {strCmpOperator} ";
                            }
                            else
                                strResult = $"{sec1.TableName}.{sec1.Name} {strCmpOperator} ";
                        }
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = left;
                            cmd.Parameters.Add(param1);

                            strResult = $"@{param1.ParameterName} {strCmpOperator} ";
                        }

                        if (right is CryEntityCreateQueryColumnInfo sec2)
                        {
                            if (sec2.Manipulator != CryEntityColumnManipulator.None)
                            {
                                if (string.IsNullOrEmpty(sec2.TableName) || string.IsNullOrEmpty(sec2.Name))
                                    strResult += $"{GetAggregateFunction(sec2.Manipulator)}(*)";
                                else
                                    strResult += $"{GetAggregateFunction(sec2.Manipulator)}({sec2.TableName}.{sec2.Name})";
                            }
                            else
                                strResult += $"{sec2.TableName}.{sec2.Name}";
                        }
                        else
                        {
                            var param1 = cmd.CreateParameter();
                            param1.ParameterName = $"{strVarName}{idx++}";
                            param1.Value = right;
                            cmd.Parameters.Add(param1);

                            strResult += $"@{param1.ParameterName}";
                        }

                        return strResult;
                    }
            }
        }

        protected virtual string GetSingleWhere(CryEntityQueryWhereOnInfo info, DbCommand cmd, string strVarName, ref int idx)
        {
            return info.Operator switch
            {
                CryEntityQueryOperator.Equal => GetComparison(info.First, info.Second, info.Third, "=", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.NullSafeEqual => GetComparison(info.First, info.Second, info.Third, "<=>", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.NotEqual => GetComparison(info.First, info.Second, info.Third, "<>", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.GreaterThen => GetComparison(info.First, info.Second, info.Third, ">", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.GreaterOrEqual => GetComparison(info.First, info.Second, info.Third, ">=", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.LesserThen => GetComparison(info.First, info.Second, info.Third, "<", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.LesserOrEqual => GetComparison(info.First, info.Second, info.Third, "<=", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.Between => GetComparison(info.First, info.Second, info.Third, "BETWEEN", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.Is => GetComparison(info.First, info.Second, info.Third, "IS", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.IsNot => GetComparison(info.First, info.Second, info.Third, "IS NOT", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.IsNull => GetComparison(info.First, info.Second, info.Third, "IS NULL", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.IsNotNull => GetComparison(info.First, info.Second, info.Third, "IS NOT NULL", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.Like => GetComparison(info.First, info.Second, info.Third, "LIKE", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.NotBetween => GetComparison(info.First, info.Second, info.Third, "BETWEEN", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.NotLike => GetComparison(info.First, info.Second, info.Third, "NOT LIKE", cmd, strVarName, info.Operator, ref idx),
                CryEntityQueryOperator.StringCompare => GetComparison(info.First, info.Second, info.Third, "STRCMP", cmd, strVarName, info.Operator, ref idx),
                _ => throw new ArgumentException("invalid operator type", nameof(info)),
            };
        }

        protected virtual string CreateOrderBy(CryEntityCreateQueryInfo info)
        {
            var cur = info.OrderBy;
            string strResult = "ORDER BY ";

            while (cur != null)
            {
                strResult += cur.Column.TableName + "." + cur.Column.Name;

                switch (cur.OrderDirection)
                {
                    case CryEntityOrderDirection.Ascending:
                        strResult += " ASC";
                        break;
                    case CryEntityOrderDirection.Descending:
                        strResult += " DESC";
                        break;
                }

                strResult += ", ";
                cur = cur.Next;
            }

            if (strResult.Length > 2)
                return strResult.Remove(strResult.Length - 2, 2);

            return string.Empty;
        }

        protected virtual string CreateGroupBy(CryEntityCreateQueryInfo info)
        {
            var cur = info.GroupBy.GroupInfo;
            string strResult = "GROUP BY ";

            while (cur != null)
            {
                strResult += cur.ColumnInfo.TableName + "." + cur.ColumnInfo.Name + ", ";
                cur = cur.Next;
            }

            if (strResult.Length > 2)
                strResult = strResult.Remove(strResult.Length - 2, 2);

            if (info.GroupBy.Having != null)
            {
                strResult += " HAVING ";

                int groupByIdx = 0;

                strResult += CreateWhere(info.GroupBy.Having, info.Command, "groupByVar", ref groupByIdx);
            }

            return strResult;
        }
    }
}
