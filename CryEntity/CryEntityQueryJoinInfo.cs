﻿
namespace CryEntity
{
    public sealed class CryEntityQueryJoinInfo
    {
        internal CryEntityTableInfo TableInfo { get; }

        public string Table => TableInfo.Name;
        public CryEntityJoinType JoinType { get; internal set; }
        public CryEntityQueryWhereOnInfo On { get; internal set; }

        internal CryEntityQueryJoinInfo()
        { }

        internal CryEntityQueryJoinInfo(CryEntityTableInfo table, CryEntityQueryWhereOnInfo on)
        {
            TableInfo = table;
            On = on;
        }
    }
}
