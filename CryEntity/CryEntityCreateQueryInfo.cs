﻿using System.Collections.Generic;
using System.Data.Common;

namespace CryEntity
{
    public sealed class CryEntityCreateQueryInfo
    {
        internal CryEntityTableInfo TableInfo { get; }

        public string TableName => TableInfo.Name;

        public ICollection<CryEntityCreateQueryColumnInfo> Columns { get; internal set; }

        public DbCommand Command { get; internal set; }

        public ICollection<CryEntityQueryJoinInfo> Joins { get; internal set; }

        public CryEntityQueryWhereOnInfo Where { get; internal set; }

        public CryEntityOrderInfo OrderBy { get; internal set; }

        public CryEntityGroupHavingInfo GroupBy { get; internal set; }

        public long Limit { get; internal set; }
        public long Offset { get; internal set; }
        public bool IsDistinct { get; internal set; }

        public CryEntityColumnManipulator Manipulator { get; internal set; }

        internal CryEntityCreateQueryInfo(CryEntityTableInfo tableInfo)
        {
            TableInfo = tableInfo;
            Columns = new List<CryEntityCreateQueryColumnInfo>();
            Joins = new List<CryEntityQueryJoinInfo>();
        }

        public CryEntityCreateQueryColumnInfo GetPrimaryKey()
        {
            foreach (var primary in Columns)
            {
                if (primary.ColumnInfo.IsPrimaryKey)
                    return primary;
            }

            foreach (var primary in TableInfo.Columns)
            {
                if (primary.Value.IsPrimaryKey)
                    return new CryEntityCreateQueryColumnInfo(primary.Value);
            }

            return null;
        }
    }
}
