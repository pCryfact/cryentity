﻿using System;

namespace CryEntity
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CryEntityOrderByAttribute : Attribute
    {
        public CryEntityOrderDirection OrderDirection { get; }

        public CryEntityOrderByAttribute(CryEntityOrderDirection orderDirection)
        {
            OrderDirection = orderDirection;
        }
    }
}
