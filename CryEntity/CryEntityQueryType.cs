﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryEntity
{
    public enum CryEntityQueryType
    {
        Select,
        Insert,
        Update,
        Delete,
    }
}
