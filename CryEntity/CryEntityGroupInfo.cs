﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryEntity
{
    public class CryEntityGroupInfo
    {
        public CryEntityCreateQueryColumnInfo ColumnInfo { get; internal set; }
        public CryEntityGroupInfo Next { get; internal set; }

        internal CryEntityGroupInfo()
        { }
    }
}
