﻿using System;

namespace CryEntity
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CryEntityColumnNameAttribute : Attribute
    {
        public string Name { get; }

        public CryEntityColumnNameAttribute(string strName)
        {
            Name = strName;
        }
    }
}
