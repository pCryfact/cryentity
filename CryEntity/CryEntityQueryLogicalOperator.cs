﻿
namespace CryEntity
{
    public enum CryEntityQueryLogicalOperator
    {
        And,
        Or,
        Xor,
        Custom,
    }
}
