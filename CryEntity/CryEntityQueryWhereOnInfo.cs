﻿
namespace CryEntity
{
    public sealed class CryEntityQueryWhereOnInfo
    {
        public object First { get; internal set; }
        public object Second { get; internal set; }
        public object Third { get; internal set; }

        public CryEntityQueryOperator Operator { get; internal set; }
        public CryEntityQueryLogicalOperator NextOperator { get; internal set; }
        public CryEntityQueryWhereOnInfo Next { get; internal set; }

        internal CryEntityQueryWhereOnInfo()
        { }

        internal CryEntityQueryWhereOnInfo(CryEntityCreateQueryColumnInfo column, CryEntityQueryOperator op)
        {
            First = column;
            Operator = op;
            NextOperator = CryEntityQueryLogicalOperator.Custom;
            Next = null;
        }
    }
}
