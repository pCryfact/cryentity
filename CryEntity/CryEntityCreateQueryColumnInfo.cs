﻿using System;
using System.Data.Common;

namespace CryEntity
{
    public enum CryEntityColumnManipulator
    {
        None,
        Average,
        Min,
        Max,
        Count,
        Sum,
    }

    public class CryEntityCreateQueryColumnInfo
    {
        internal CryEntityColumnInfo ColumnInfo { get; }

        internal CryEntityTableInfo TableInfo { get; set; }

        internal int Index { get; set; }

        internal virtual Type PropertyType { get => ColumnInfo.PropertyInfo.PropertyType; set => throw new NotSupportedException(); }

        public virtual string TableName => TableInfo == null ? ColumnInfo.TableInfo.Name : TableInfo.Name;
        public virtual string Name => ColumnInfo.ColumnName;
        public virtual string Alias { get => ColumnInfo.Alias; }
        public DbParameter Parameter { get; internal set; }
        public CryEntityColumnManipulator Manipulator { get; internal set; }

        internal  CryEntityCreateQueryColumnInfo(CryEntityColumnInfo columnInfo)
        {
            ColumnInfo = columnInfo;
        }

        internal CryEntityCreateQueryColumnInfo(CryEntityColumnInfo columnInfo, CryEntityColumnManipulator manipulator)
        {
            ColumnInfo = columnInfo;
            Manipulator = manipulator;
        }
    }

    internal class CryEntityUnknownColumnInfo : CryEntityCreateQueryColumnInfo
    {
        private Type m_type;

        public override string TableName => string.Empty;
        public override string Name => string.Empty;
        public override string Alias { get; }

        internal override Type PropertyType { get => m_type; set => m_type = value; }

        internal CryEntityUnknownColumnInfo(string alias, CryEntityColumnManipulator manipulator)
            : base(null, manipulator)
        {
            Alias = alias;
        }
    }
}
