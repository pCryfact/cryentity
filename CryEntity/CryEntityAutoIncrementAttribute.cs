﻿using System;

namespace CryEntity
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CryEntityAutoIncrementAttribute : Attribute
    { }
}
