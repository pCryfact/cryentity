﻿
namespace CryEntity
{
    public enum CryEntityQueryOperator
    {
        Equal,
        NullSafeEqual,
        NotEqual,
        LesserThen,
        LesserOrEqual,
        GreaterThen,
        GreaterOrEqual,
        Between,
        Is,
        IsNot,
        IsNotNull,
        IsNull,
        Like,
        NotBetween,
        NotLike,
        StringCompare,
    }
}
