﻿
namespace CryEntity
{
    public sealed class CryEntityOrderInfo
    {
        public CryEntityOrderDirection OrderDirection { get; internal set; }
        public CryEntityCreateQueryColumnInfo Column { get; internal set; }

        public CryEntityOrderInfo Next { get; internal set; }

        internal CryEntityOrderInfo(CryEntityOrderDirection orderDirection, CryEntityCreateQueryColumnInfo columnInfo)
        {
            Column = columnInfo;
            OrderDirection = orderDirection;
        }

        internal CryEntityOrderInfo(CryEntityOrderDirection orderDirection)
        {
            OrderDirection = orderDirection;
        }
    }
}
