﻿
namespace CryEntity
{
    public sealed class CryEntityQueryStatement
    {
        public bool IsUnionAll { get; internal set; }
        public bool IsUnion { get; internal set; }
        public bool IsExcept { get; internal set; }
        public CryEntityCreateQueryInfo QueryInfo { get; internal set; }
        public CryEntityQueryStatement Next { get; internal set; }

        internal CryEntityQueryStatement()
        { }
    }
}
