﻿using System;

namespace CryEntity
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Class)]
    public sealed class CryEntityForeignKeyAttribute : Attribute
    { 
        public string ColumnName { get; }

        public CryEntityJoinType JoinType { get; }

        public CryEntityForeignKeyAttribute(string strColumnName, CryEntityJoinType joinType)
        {
            ColumnName = strColumnName;
            JoinType = joinType;
        }

        public CryEntityForeignKeyAttribute(string strColumnName)
        {
            ColumnName = strColumnName;
            JoinType = CryEntityJoinType.Default;
        }
    }
}
