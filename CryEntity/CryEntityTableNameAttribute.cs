﻿using System;

namespace CryEntity
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class CryEntityTableNameAttribute : Attribute
    {
        public string Name { get; }

        public CryEntityTableNameAttribute(string strName)
        {
            Name = strName;
        }
    }
}
