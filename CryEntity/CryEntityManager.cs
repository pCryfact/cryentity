﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace CryEntity
{
    public class CryEntityManager
    {
        public static Type PreferredCollection { get; set; }
        public static Type PreferredList { get; set; }
        public static Type PreferredDictionary { get; set; }
        public static int MaxPullCount { get; set; }

        internal static Dictionary<Type, CryEntityTableInfo> TableInfo { get; }

        internal ICryEntityQueryBuilder QueryBuilder => m_queryBuilder;
        internal DbConnection Connection => m_connection;
        internal ICryEntityDataBase DataBase { get; }

        private readonly DbConnection m_connection; // default synchronous connection
        private readonly ICryEntityQueryBuilder m_queryBuilder;

        static CryEntityManager()
        {
            TableInfo = new Dictionary<Type, CryEntityTableInfo>();
            PreferredCollection = typeof(List<>);
            PreferredList = typeof(List<>);
            PreferredDictionary = typeof(Dictionary<,>);
            MaxPullCount = 100;
        }

        public CryEntityManager(ICryEntityDataBase db, ICryEntityQueryBuilder queryBuilder)
        {
            DataBase = db;
            m_connection = DataBase.CreateConnection();
            m_queryBuilder = queryBuilder;

            m_connection.Open();
        }

        public CryEntityManager(ICryEntityDataBase db)
        {
            DataBase = db;
            m_connection = DataBase.CreateConnection();
            m_queryBuilder = new CryEntityMySqlQueryBuilder();

            m_connection.Open();
        }

        internal static CryEntityTableInfo CreateOrGetTableInfo(Type type)
        {
            lock (TableInfo)
            {
                if (!TableInfo.TryGetValue(type, out var tableInfo))
                {
                    if (type.GetCustomAttribute<CryEntityTableNameAttribute>() == null)
                        return null;

                    tableInfo = new CryEntityTableInfo(type);
                    TableInfo.Add(type, tableInfo);
                }

                return tableInfo;
            }
        }

        public IQueryable<T> Table<T>()
        {
            return new CryEntityQueryableCollection<T>(new CryEntityQueryableProvider(this, typeof(T), m_connection, false));
        }

        public IQueryable<T> TableLazy<T>()
        {
            return new CryEntityQueryableCollection<T>(new CryEntityQueryableProvider(this, typeof(T), m_connection, true));
        }

        public ICollection<T> Find<T>()
        {
            return Find<T>(m_connection, m_queryBuilder, 0, 0);
        }

        public ICollection<T> Find<T>(int offset, int limit)
        {
            return Find<T>(m_connection, m_queryBuilder, offset, limit);
        }

        public ICollection<T> Find<T>(CryEntityOrderDirection direction, string columnName)
        {
            return Find<T>(m_connection, m_queryBuilder, direction, columnName, 0, 0);
        }

        public ICollection<T> Find<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            return Find<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit);
        }

        public ICollection<T> Find<T>(params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return Find<T>(m_connection, m_queryBuilder, 0, 0, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICollection<T> Find<T>(int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return Find<T>(m_connection, m_queryBuilder, offset, limit, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICollection<T> Find<T>(CryEntityOrderDirection direction, string columnName, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return Find<T>(m_connection, m_queryBuilder, direction, columnName, 0, 0, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICollection<T> Find<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return Find<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICollection<T> Find<T>(params KeyValuePair<string, object>[] keys)
        {
            return Find<T>(m_connection, m_queryBuilder, 0, 0, keys);
        }

        public ICollection<T> Find<T>(CryEntityOrderDirection direction, string columnName, params KeyValuePair<string, object>[] keys)
        {
            return Find<T>(m_connection, m_queryBuilder, direction, columnName, 0, 0, keys);
        }

        public ICollection<T> Find<T>(int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return Find<T>(m_connection, m_queryBuilder, offset, limit, keys);
        }

        public ICollection<T> Find<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return Find<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit, keys);
        }

        public Task<ICollection<T>> FindAsync<T>()
        {
            return FindAsync<T>(0, 0, CancellationToken.None);
        }

        public Task<ICollection<T>> FindAsync<T>(CancellationToken cancellationToken)
        {
            return FindAsync<T>(0, 0, cancellationToken);
        }

        public Task<ICollection<T>> FindAsync<T>(int offset, int limit)
        {
            return FindAsync<T>(offset, limit, CancellationToken.None);
        }

        public Task<ICollection<T>> FindAsync<T>(int offset, int limit, CancellationToken cancellationToken)
        {
            Func<ICollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = Find<T>(dbConnection, m_queryBuilder, offset, limit);
                dbConnection.Close();
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICollection<T>>(action);

            task.Start();

            return task;
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName)
        {
            return FindAsync<T>(direction, columnName, 0, 0, CancellationToken.None);
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, CancellationToken cancellationToken)
        {
            return FindAsync<T>(direction, columnName, 0, 0, cancellationToken);
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            return FindAsync<T>(direction, columnName, offset, limit, CancellationToken.None);
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, CancellationToken cancellationToken)
        {
            Func<ICollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = Find<T>(dbConnection, m_queryBuilder, direction, columnName, offset, limit);
                dbConnection.Close();
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICollection<T>>(action);

            task.Start();

            return task;
        }

        public Task<ICollection<T>> FindAsync<T>(params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(offset, limit, CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(int offset, int limit, CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(offset, limit, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(direction, columnName, 0, 0, CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(direction, columnName, 0, 0, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(direction, columnName, offset, limit, CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindAsync<T>(direction, columnName, offset, limit, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICollection<T>> FindAsync<T>(params KeyValuePair<string, object>[] keys)
        {
            return FindAsync<T>(0, 0, CancellationToken.None, keys);
        }

        public Task<ICollection<T>> FindAsync<T>(CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            return FindAsync<T>(0, 0, cancellationToken, keys);
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, params KeyValuePair<string, object>[] keys)
        {
            return FindAsync<T>(direction, columnName, 0, 0, CancellationToken.None, keys);
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            return FindAsync<T>(direction, columnName, 0, 0, cancellationToken, keys);
        }

        public Task<ICollection<T>> FindAsync<T>(int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return FindAsync<T>(offset, limit, CancellationToken.None, keys);
        }

        public Task<ICollection<T>> FindAsync<T>(int offset, int limit, CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            Func<ICollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = Find<T>(dbConnection, m_queryBuilder, offset, limit, keys);
                dbConnection.Close();
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICollection<T>>(action);

            task.Start();

            return task;
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return FindAsync<T>(direction, columnName, offset, limit, CancellationToken.None, keys);
        }

        public Task<ICollection<T>> FindAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            Func<ICollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = Find<T>(dbConnection, m_queryBuilder, direction, columnName, offset, limit, keys);
                dbConnection.Close();
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICollection<T>>(action);

            task.Start();

            return task;
        }

        public ICryEntityLazyCollection<T> FindLazy<T>()
        {
            return FindLazy<T>(m_connection, m_queryBuilder, 0, 0);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(int offset, int limit)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, offset, limit);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(CryEntityOrderDirection direction, string columnName)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, direction, columnName, 0, 0);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazy<T>(m_connection, m_queryBuilder, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazy<T>(m_connection, m_queryBuilder, offset, limit, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(CryEntityOrderDirection direction, string columnName, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazy<T>(m_connection, m_queryBuilder, direction, columnName, 0, 0, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazy<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(params KeyValuePair<string, object>[] keys)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, 0, 0, keys);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(CryEntityOrderDirection direction, string columnName, params KeyValuePair<string, object>[] keys)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, direction, columnName, 0, 0, keys);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, offset, limit, keys);
        }

        public ICryEntityLazyCollection<T> FindLazy<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return FindLazy<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>()
        {
            return FindLazyAsync<T>(0, 0, CancellationToken.None);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CancellationToken cancellationToken)
        {
            return FindLazyAsync<T>(m_connection, m_queryBuilder, 0, 0, cancellationToken);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(int offset, int limit)
        {
            return FindLazyAsync<T>(offset, limit, CancellationToken.None);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(int offset, int limit, CancellationToken cancellationToken)
        {
            Func<ICryEntityLazyCollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = FindLazy<T>(dbConnection, m_queryBuilder, offset, limit);
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICryEntityLazyCollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICryEntityLazyCollection<T>>(action);

            task.Start();

            return task;
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName)
        {
            return FindLazyAsync<T>(direction, columnName, 0, 0, CancellationToken.None);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, CancellationToken cancellationToken)
        {
            return FindLazyAsync<T>(direction, columnName, 0, 0, cancellationToken);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            return FindLazyAsync<T>(direction, columnName, offset, limit, CancellationToken.None);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, CancellationToken cancellationToken)
        {
            Func<ICryEntityLazyCollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = FindLazy<T>(dbConnection, m_queryBuilder, direction, columnName, offset, limit);
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICryEntityLazyCollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICryEntityLazyCollection<T>>(action);

            task.Start();

            return task;
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(0, 0, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(offset, limit, CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(int offset, int limit, CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(offset, limit, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray().ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(direction, columnName, 0, 0, CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(direction, columnName, 0, 0, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(direction, columnName, offset, limit, CancellationToken.None, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, CancellationToken cancellationToken, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazyAsync<T>(direction, columnName, offset, limit, cancellationToken, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(params KeyValuePair<string, object>[] keys)
        {
            return FindLazyAsync<T>(0, 0, CancellationToken.None, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            return FindLazyAsync<T>(0, 0, cancellationToken, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, params KeyValuePair<string, object>[] keys)
        {
            return FindLazyAsync<T>(direction, columnName, 0, 0, CancellationToken.None, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            return FindLazyAsync<T>(direction, columnName, 0, 0, cancellationToken, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return FindLazyAsync<T>(offset, limit, CancellationToken.None, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(int offset, int limit, CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            Func<ICryEntityLazyCollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = FindLazy<T>(dbConnection, m_queryBuilder, offset, limit, keys);
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICryEntityLazyCollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICryEntityLazyCollection<T>>(action);

            task.Start();

            return task;
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            return FindLazyAsync<T>(m_connection, m_queryBuilder, direction, columnName, offset, limit, CancellationToken.None, keys);
        }

        public Task<ICryEntityLazyCollection<T>> FindLazyAsync<T>(CryEntityOrderDirection direction, string columnName, int offset, int limit, CancellationToken cancellationToken, params KeyValuePair<string, object>[] keys)
        {
            Func<ICryEntityLazyCollection<T>> action = () =>
            {
                var dbConnection = DataBase.CreateConnection();
                dbConnection.Open();
                var result = FindLazy<T>(dbConnection, m_queryBuilder, direction, columnName, offset, limit, keys);
                return result;
            };

            var task = cancellationToken.IsCancellationRequested ? new Task<ICryEntityLazyCollection<T>>(action, cancellationToken, TaskCreationOptions.None) : new Task<ICryEntityLazyCollection<T>>(action);

            task.Start();

            return task;
        }

        public bool Store<T>(T obj)
        {
            var curTable = CreateOrGetTableInfo(typeof(T));

            using var transaction = m_connection.BeginTransaction();

            if (!StoreValues(obj, curTable, transaction))
            {
                transaction.Rollback();
                return false;
            }

            transaction.Commit();

            return true;
        }

        public object Persist<T>(T obj)
        {
            var curTable = CreateOrGetTableInfo(typeof(T));

            using var transaction = m_connection.BeginTransaction();

            var result = PersistValues(obj, curTable, transaction);

            if (result == null)
                transaction.Rollback();
            else
                transaction.Commit();

            return result;
        }

        public bool Delete<T>(T obj)
        {
            var curTable = CreateOrGetTableInfo(typeof(T));

            List<CryEntityTableInfo> tableInfos = new List<CryEntityTableInfo>();

            while (curTable != null)
            {
                tableInfos.Add(curTable);
                curTable = curTable.Base;
            }

            tableInfos.Reverse();

            using var transaction = m_connection.BeginTransaction();

            foreach (var table in tableInfos)
            {
                using var cmd = m_connection.CreateCommand();

                var q = new CryEntityQueryStatement();
                q .QueryInfo = new CryEntityCreateQueryInfo(table);
                q .QueryInfo.Columns = GetColumns(table);
                q .QueryInfo.Command = cmd;
                q.QueryInfo.Where = GetAllPrimaryKeyWheres(GetKeyValuePairs(obj, table), table);

                cmd.CommandText = m_queryBuilder.CreateDeleteQuery(q);
                cmd.Transaction = transaction;

                try
                {
                    if (cmd.ExecuteNonQuery() != 1)
                    {
                        transaction.Rollback();
                        return false;
                    }
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }

            transaction.Commit();

            return true;
        }

        private bool StoreValues(object obj, CryEntityTableInfo table, DbTransaction transaction)
        {
            while (table != null)
            {
                foreach (var foreign in table.ForeignKeys)
                {
                    if (foreign.PrimaryKey.TableInfo == table.Base)
                        continue;

                    var myObj = foreign.ForeignKey.PropertyInfo.GetValue(obj);

                    if (foreign.IsCollection)
                    {
                        ICollection collection = myObj as ICollection;

                        if (collection != null)
                        {
                            foreach (var tmpObj in collection)
                            {
                                if (!StoreValues(tmpObj, foreign.ForeignKey.TableInfo, transaction))
                                    return false;
                            }
                        }
                    }
                    else if (foreign.IsDictionary)
                    {
                        IDictionary collection = myObj as IDictionary;

                        if (collection != null)
                        {
                            foreach (var tmpObj in collection.Values)
                            {
                                if (!StoreValues(tmpObj, foreign.ForeignKey.TableInfo, transaction))
                                    return false;
                            }
                        }
                    }
                    else if (!StoreValues(myObj, foreign.PrimaryKey.TableInfo, transaction))
                        return false;
                }

                Dictionary<CryEntityColumnInfo, object> dictValues = new Dictionary<CryEntityColumnInfo, object>();

                table.GetValues(obj, table.Type, dictValues);

                using var cmd = m_connection.CreateCommand();

                var q = new CryEntityQueryStatement();
                q.QueryInfo = new CryEntityCreateQueryInfo(table);
                q.QueryInfo.Columns = GetColumns(dictValues, cmd);
                q.QueryInfo.Command = cmd;
                q.QueryInfo.Where = GetAllPrimaryKeyWheres(GetKeyValuePairs(obj, table), table);

                cmd.CommandText = m_queryBuilder.CreateUpdateQuery(q);

                if (!string.IsNullOrEmpty(cmd.CommandText))
                {
                    cmd.Transaction = transaction;

                    try
                    {
                        if (cmd.ExecuteNonQuery() != 1)
                            return false;
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }

                table = table.Base;
            }

            return true;
        }

        private object PersistValues(object obj, CryEntityTableInfo myTable, DbTransaction transaction)
        {
            object result = null;

            List<CryEntityTableInfo> tableInfos = new List<CryEntityTableInfo>();

            while (myTable != null)
            {
                tableInfos.Add(myTable);
                myTable = myTable.Base;
            }

            tableInfos.Reverse();

            Dictionary<string, object> foreignKeyValues = new Dictionary<string, object>();

            foreach (var table in tableInfos)
            {
                Dictionary<CryEntityColumnInfo, object> dictValues = new Dictionary<CryEntityColumnInfo, object>();

                foreach (var foreign in table.ForeignKeys)
                {
                    if (foreignKeyValues.TryGetValue(foreign.PrimaryKey.ColumnName, out var dictValue))
                        dictValues.Add(foreign.ForeignKey, dictValue);

                    if (foreign.PrimaryKey.TableInfo == table.Base)
                        continue;

                    var myObj = foreign.ForeignKey.PropertyInfo.GetValue(obj);

                    if (foreign.IsCollection)
                    {
                        ICollection collection = myObj as ICollection;

                        foreach (var tmpObj in collection)
                        {
                            result = PersistValues(tmpObj, foreign.ForeignKey.TableInfo, transaction);

                            if (result == null)
                                return null;
                        }
                    }
                    else if (foreign.IsDictionary)
                    {
                        IDictionary collection = myObj as IDictionary;

                        foreach (var tmpObj in collection.Values)
                        {
                            result = PersistValues(tmpObj, foreign.ForeignKey.TableInfo, transaction);

                            if (result == null)
                                return null;
                        }
                    }
                    else
                    {
                        result = PersistValues(myObj, foreign.PrimaryKey.TableInfo, transaction);

                        if (result == null)
                            return null;

                        dictValues.Add(foreign.ForeignKey, result);
                    }
                }

                table.GetValues(obj, table.Type, dictValues);

                using var cmd = m_connection.CreateCommand();

                var q = new CryEntityQueryStatement();
                q.QueryInfo = new CryEntityCreateQueryInfo(table);
                q.QueryInfo.Columns = GetColumns(dictValues, cmd);
                q.QueryInfo.Command = cmd;


                cmd.CommandText = m_queryBuilder.CreateInsertQuery(q);

                if (!string.IsNullOrEmpty(cmd.CommandText))
                {
                    cmd.Transaction = transaction;

                    try
                    {
                        if (cmd.ExecuteNonQuery() != 1)
                            return null;

                        if (table.PrimaryKey.IsAutoIncrement)
                        {
                            using var cmd2 = m_connection.CreateCommand();

                            cmd2.CommandText = m_queryBuilder.CreateGetLastInsertedIdQuery();

                            result = cmd2.ExecuteScalar();

                            if (result == null)
                                return null;

                            foreignKeyValues.Add(table.PrimaryKey.ColumnName, result);

                            if (!table.PrimaryKey.IsIgnored)
                                table.PrimaryKey.PropertyInfo.SetValue(obj, Convert.ChangeType(result, table.PrimaryKey.PropertyInfo.PropertyType));
                        }
                        else
                            result = table.PrimaryKey.PropertyInfo.GetValue(obj);
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                }
                else
                    continue;
            }

            return result;
        }

        private void GetAllTableInfos(CryEntityTableReferenceInfo refTableInfo, List<CryEntityTableReferenceInfo> output)
        {
            var tableInfo = refTableInfo.TableInfo;

            while (tableInfo != null)
            {
                foreach (var refTable in tableInfo.ReferencedTables)
                    GetAllTableInfos(refTable, output);

                output.Add(refTableInfo);
                tableInfo = tableInfo.Base;
                refTableInfo = null;
            }
        }

        internal static ICollection<CryEntityCreateQueryColumnInfo> GetAllColumns(params CryEntityTableInfo[] tableInfos)
        {
            Dictionary<string, CryEntityCreateQueryColumnInfo> result = new Dictionary<string, CryEntityCreateQueryColumnInfo> ();

            foreach (var tableInfo in tableInfos)
                GetAllColumns(tableInfo, result);

            return result.Values.ToList();
        }

        private static void GetAllColumns(CryEntityTableInfo tableInfo, Dictionary<string, CryEntityCreateQueryColumnInfo> output)
        {
            var currTable = tableInfo;

            while (currTable != null)
            {
                foreach (var column in currTable.Columns)
                {
                    string strKey = currTable.Name + "." + column.Key;

                    if (!output.ContainsKey(strKey))
                        output.Add(strKey, new CryEntityCreateQueryColumnInfo(column.Value));
                }

                foreach (var foreign in currTable.ForeignKeys)
                {
                    if (foreign.PrimaryKey.TableInfo == currTable.Base)
                        continue;

                    foreach (var column in foreign.PrimaryKey.TableInfo.Columns)
                    {
                        string strKey = column.Value.TableInfo.Name + "." + column.Key;
                        if (!output.ContainsKey(strKey))
                            output.Add(strKey, new CryEntityCreateQueryColumnInfo(column.Value));

                        if (foreign.PrimaryKey.TableInfo == currTable)
                            GetAllColumns(foreign.ForeignKey.TableInfo, output);
                        else
                            GetAllColumns(foreign.PrimaryKey.TableInfo, output);
                    }
                }

                currTable = currTable.Base;
            }
        }

        private static List<CryEntityCreateQueryColumnInfo> GetColumns(Dictionary<CryEntityColumnInfo, object> values, DbCommand cmd)
        {
            List<CryEntityCreateQueryColumnInfo> result = new List<CryEntityCreateQueryColumnInfo>();
            int i = 0;

            foreach (var column in values)
            {
                var param = cmd.CreateParameter();

                param.ParameterName = $"var{i++}";
                param.Value = column.Value;

                result.Add(new CryEntityCreateQueryColumnInfo(column.Key) { Parameter = param });

                cmd.Parameters.Add(param);
            }

            return result;
        }

        private static List<CryEntityCreateQueryColumnInfo> GetColumns(CryEntityTableInfo tableInfo)
        {
            List<CryEntityCreateQueryColumnInfo> result = new List<CryEntityCreateQueryColumnInfo>();

            foreach (var column in tableInfo.Columns)
                result.Add(new CryEntityCreateQueryColumnInfo(column.Value));

            return result;
        }

        internal static ICollection<CryEntityQueryJoinInfo> GetAllJoins(CryEntityTableInfo tableInfo)
        {
            Dictionary<Type, Dictionary<string, CryEntityQueryJoinInfo>> result = new Dictionary<Type, Dictionary<string, CryEntityQueryJoinInfo>>();

            GetAllJoins(tableInfo, result);

            List<CryEntityQueryJoinInfo> lst = new List<CryEntityQueryJoinInfo>();

            foreach (var l in result)
                lst.AddRange(l.Value.Values);

            return lst;
        }

        private static void GetAllJoins(CryEntityTableInfo tableInfo, Dictionary<Type, Dictionary<string, CryEntityQueryJoinInfo>> output)
        {
            var currTable = tableInfo;

            while (currTable != null)
            {
                if (!output.TryGetValue(currTable.Type, out var lst))
                {
                     lst = new Dictionary<string, CryEntityQueryJoinInfo>();
                    output.Add(currTable.Type, lst);
                }

                foreach (var column in currTable.ForeignKeys)
                {
                    if (!lst.ContainsKey(column.ForeignKey.ColumnName))
                    {
                        bool isReverse = column.IsCollection || column.IsDictionary;

                        var foreign = new CryEntityCreateQueryColumnInfo(column.ForeignKey);

                        if (isReverse)
                            foreign.TableInfo = column.PrimaryKey.TableInfo;

                        var on = new CryEntityQueryWhereOnInfo(foreign, CryEntityQueryOperator.Equal);
                        CryEntityQueryWhereOnInfo currOn = null;

                        if (currOn == null)
                        {
                            currOn = on;

                            if (isReverse)
                                currOn.Second = new CryEntityCreateQueryColumnInfo(column.PrimaryKey) { TableInfo = column.ForeignKey.TableInfo };
                            else
                                currOn.Second = new CryEntityCreateQueryColumnInfo(column.PrimaryKey);

                            currOn.NextOperator = CryEntityQueryLogicalOperator.And;
                        }
                        else
                        {
                            currOn.Next = new CryEntityQueryWhereOnInfo(foreign, CryEntityQueryOperator.Equal);

                            if (isReverse)
                                currOn.Second = new CryEntityCreateQueryColumnInfo(column.PrimaryKey) { TableInfo = column.ForeignKey.TableInfo };
                            else
                                currOn.Second = new CryEntityCreateQueryColumnInfo(column.PrimaryKey);

                            currOn.NextOperator = CryEntityQueryLogicalOperator.And;
                            currOn = currOn.Next;
                        }

                        lst.Add(column.ForeignKey.ColumnName, new CryEntityQueryJoinInfo(column.PrimaryKey.TableInfo, on) { JoinType = column.JoinType });
                    }

                    GetAllJoins(column.PrimaryKey.TableInfo, output);
                }

                currTable = currTable.Base;
            }
        }

        private static CryEntityQueryWhereOnInfo GetAllPrimaryKeyWheres(IEnumerable<KeyValuePair<string, object>> keys, CryEntityTableInfo tableInfo)
        {
            CryEntityQueryWhereOnInfo result = null;
            CryEntityQueryWhereOnInfo current = null;

            foreach (var key in keys)
            {
                if (tableInfo.Columns.TryGetValue(key.Key, out var column))
                {
                    if (result == null)
                    {
                        result = new CryEntityQueryWhereOnInfo(new CryEntityCreateQueryColumnInfo(column), CryEntityQueryOperator.Equal);
                        result.Second = key.Value;
                        current = result;
                    }
                    else
                    {
                        current.Next = new CryEntityQueryWhereOnInfo(new CryEntityCreateQueryColumnInfo(column), CryEntityQueryOperator.Equal);
                        result.Second = key.Value;
                        current = current.Next;
                    }
                }
            }

            return result;
        }

        private static IEnumerable<KeyValuePair<string, object>> GetKeyValuePairs(object[] keys, CryEntityTableInfo tableInfo)
        {
            for (int i = 0; i < keys.Length; ++i)
                yield return new KeyValuePair<string, object>(tableInfo.PrimaryKey.ColumnName, keys[i]);
        }

        private static IEnumerable<KeyValuePair<string, object>> GetKeyValuePairs<T>(T obj, CryEntityTableInfo tableInfo)
        {
            yield return new KeyValuePair<string, object>(tableInfo.PrimaryKey.ColumnName, tableInfo.PrimaryKey.PropertyInfo.GetValue(obj));
        }

        private static CryEntityCreateQueryColumnInfo GetColumnByName(CryEntityTableInfo tableInfo, string strName)
        {
            var currTable = tableInfo;

            while (currTable != null)
            {
                foreach (var column in currTable.Columns)
                {
                    if (column.Key == strName)
                        return new CryEntityCreateQueryColumnInfo(column.Value);
                }

                foreach (var foreign in currTable.ForeignKeys)
                {
                    if (foreign.PrimaryKey.TableInfo == currTable.Base)
                        continue;

                    foreach (var column in foreign.PrimaryKey.TableInfo.Columns)
                    {
                        string strKey = column.Value.TableInfo.Name + "." + column.Key;

                        var result = GetColumnByName(foreign.PrimaryKey.TableInfo, strName);

                        if (result != null)
                            return result;
                    }
                }

                currTable = currTable.Base;
            }

            return null;
        }

        private static CryEntityOrderInfo GetOrderBy(CryEntityTableInfo tableInfo)
        {
            var currTable = tableInfo;

            while (currTable != null)
            {
                foreach (var column in currTable.Columns)
                {
                    if (column.Value.OrderBy)
                        return new CryEntityOrderInfo(column.Value.OrderDirection, new CryEntityCreateQueryColumnInfo(column.Value));
                }

                foreach (var foreign in currTable.ForeignKeys)
                {
                    if (foreign.PrimaryKey.TableInfo == currTable.Base)
                        continue;

                    foreach (var column in foreign.PrimaryKey.TableInfo.Columns)
                    {
                        string strKey = column.Value.TableInfo.Name + "." + column.Key;

                        var result = GetOrderBy(foreign.PrimaryKey.TableInfo);

                        if (result != null)
                            return result;
                    }
                }

                currTable = currTable.Base;
            }

            return null;
        }

        private static ICollection<T> Find<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, int offset, int limit)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, offset, limit);

            using var reader = Find<T>(connection, queryBuilder, queryCreateInfo);

            ICollection<T> result = (ICollection<T>)Activator.CreateInstance(PreferredCollection.MakeGenericType(typeof(T)));
            tableInfo.Fill(reader, result, false);

            return result;
        }

        private static ICollection<T> Find<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, direction, columnName, offset, limit);

            using var reader = Find<T>(connection, queryBuilder, queryCreateInfo);

            ICollection<T> result = (ICollection<T>)Activator.CreateInstance(PreferredCollection.MakeGenericType(typeof(T)));
            tableInfo.Fill(reader, result, false);

            return result;
        }

        private static ICollection<T> Find<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return Find<T>(connection, queryBuilder, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        private static ICollection<T> Find<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, offset, limit, keys);

            using var reader = Find<T>(connection, queryBuilder, queryCreateInfo);

            ICollection<T> result = (ICollection<T>)Activator.CreateInstance(PreferredCollection.MakeGenericType(typeof(T)));
            tableInfo.Fill(reader, result, false);

            return result;
        }

        private static ICollection<T> Find<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, direction, columnName, offset, limit, keys);

            using var reader = Find<T>(connection, queryBuilder, queryCreateInfo);

            ICollection<T> result = (ICollection<T>)Activator.CreateInstance(PreferredCollection.MakeGenericType(typeof(T)));
            tableInfo.Fill(reader, result, false);

            return result;
        }

        private static ICryEntityLazyCollection<T> FindLazy<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, int offset, int limit)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, offset, limit);

            queryCreateInfo.QueryInfo.Limit = MaxPullCount; // overwrite limit

            return new CryEntityLazyCollection<T>(tableInfo, connection, queryBuilder, queryCreateInfo, limit, PreferredList);
        }

        private static ICryEntityLazyCollection<T> FindLazy<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, direction, columnName, offset, limit);

            queryCreateInfo.QueryInfo.Limit = MaxPullCount; // overwrite limit

            return new CryEntityLazyCollection<T>(tableInfo, connection, queryBuilder, queryCreateInfo, limit, PreferredList);
        }

        private static ICryEntityLazyCollection<T> FindLazy<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, params object[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));

            return FindLazy<T>(connection, queryBuilder, GetKeyValuePairs(keys, tableInfo).ToArray());
        }

        private static ICryEntityLazyCollection<T> FindLazy<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, offset, limit, keys);

            queryCreateInfo.QueryInfo.Limit = MaxPullCount; // overwrite limit

            return new CryEntityLazyCollection<T>(tableInfo, connection, queryBuilder, queryCreateInfo, limit, PreferredList);
        }

        private static ICryEntityLazyCollection<T> FindLazy<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            var tableInfo = CreateOrGetTableInfo(typeof(T));
            var queryCreateInfo = CreateQueryInfo(tableInfo, direction, columnName, offset, limit, keys);

            queryCreateInfo.QueryInfo.Limit = MaxPullCount; // overwrite limit

            return new CryEntityLazyCollection<T>(tableInfo, connection, queryBuilder, queryCreateInfo, limit, PreferredList);
        }

        private static CryEntityQueryStatement CreateQueryInfo(CryEntityTableInfo tableInfo, int offset, int limit)
        {
            ICollection<CryEntityQueryJoinInfo> joinInfos = null;

            if (tableInfo.ForeignKeys.Count > 0)
                joinInfos = GetAllJoins(tableInfo);

            var result = new CryEntityQueryStatement();

            result.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);
            result.QueryInfo.Columns = GetAllColumns(tableInfo);
            result.QueryInfo.Joins = joinInfos;

            if (offset != 0)
                result.QueryInfo.Offset = offset;

            if (limit != 0)
                result.QueryInfo.Limit = limit;

            return result;
        }

        private static CryEntityQueryStatement CreateQueryInfo(CryEntityTableInfo tableInfo, CryEntityOrderDirection direction, string columnName, int offset, int limit)
        {
            var orderByColumn = GetColumnByName(tableInfo, columnName);

            if (orderByColumn == null)
                throw new ArgumentNullException("Column Name not found.");

            ICollection<CryEntityQueryJoinInfo> joinInfos = null;

            if (tableInfo.ForeignKeys.Count > 0)
                joinInfos = GetAllJoins(tableInfo);

            var result = new CryEntityQueryStatement();

            result.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);
            result.QueryInfo.Columns = GetAllColumns(tableInfo);
            result.QueryInfo.Joins = joinInfos;
            result.QueryInfo.OrderBy = new CryEntityOrderInfo(direction, orderByColumn);

            if (offset != 0)
                result.QueryInfo.Offset = offset;

            if (limit != 0)
                result.QueryInfo.Limit = limit;

            return result;
        }

        private static CryEntityQueryStatement CreateQueryInfo(CryEntityTableInfo tableInfo, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            ICollection<CryEntityQueryJoinInfo> joinInfos = null;

            if (tableInfo.ForeignKeys.Count > 0)
                joinInfos = GetAllJoins(tableInfo);

            var result = new CryEntityQueryStatement();

            result.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);
            result.QueryInfo.Columns = GetAllColumns(tableInfo);
            result.QueryInfo.Joins = joinInfos;
            result.QueryInfo.Where = GetAllPrimaryKeyWheres(keys, tableInfo);

            if (offset != 0)
                result.QueryInfo.Offset = offset;

            if (limit != 0)
                result.QueryInfo.Limit = limit;

            return result;
        }

        private static CryEntityQueryStatement CreateQueryInfo(CryEntityTableInfo tableInfo, CryEntityOrderDirection direction, string columnName, int offset, int limit, params KeyValuePair<string, object>[] keys)
        {
            var orderByColumn = GetColumnByName(tableInfo, columnName);

            if (orderByColumn == null)
                throw new ArgumentNullException("Column Name not found.");

            ICollection<CryEntityQueryJoinInfo> joinInfos = null;

            if (tableInfo.ForeignKeys.Count > 0)
                joinInfos = GetAllJoins(tableInfo);

            var result = new CryEntityQueryStatement();

            result.QueryInfo = new CryEntityCreateQueryInfo(tableInfo);
            result.QueryInfo.Columns = GetAllColumns(tableInfo);
            result.QueryInfo.Joins = joinInfos;
            result.QueryInfo.Where = GetAllPrimaryKeyWheres(keys, tableInfo);

            result.QueryInfo.OrderBy = new CryEntityOrderInfo(direction, orderByColumn);

            if (offset != 0)
                result.QueryInfo.Offset = offset;

            if (limit != 0)
                result.QueryInfo.Limit = limit;

            return result;
        }

        private static DbDataReader Find<T>(DbConnection connection, ICryEntityQueryBuilder queryBuilder, CryEntityQueryStatement createQueryInfo)
        {
            using var cmd = connection.CreateCommand();

            createQueryInfo.QueryInfo.Command = cmd;

            cmd.CommandText = queryBuilder.CreateSelectQuery(createQueryInfo);

            return cmd.ExecuteReader();
        }
    }
}
