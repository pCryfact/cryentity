﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryEntity
{
    public interface ICryEntityLazyCollection<T> : ICollection<T>
    {
        public int LoadedCount { get; }

        public bool AutoPulling { get; set; }

        public bool IsAutoPullFinished { get; }

        public void Pull();

        public void WaitForAutoPullingFinished();
    }
}
