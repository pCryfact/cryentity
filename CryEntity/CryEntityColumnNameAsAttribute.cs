﻿using System;

namespace CryEntity
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class CryEntityColumnNameAsAttribute : Attribute
    {
        public string Name { get; }

        public CryEntityColumnNameAsAttribute(string strName)
        {
            Name = strName;
        }
    }
}
