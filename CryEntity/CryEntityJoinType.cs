﻿using System;

namespace CryEntity
{
    [Flags]
    public enum CryEntityJoinType
    {
        Default,
        Inner = 1 << 0,
        Outer = 1 << 1,
        Cross = 1 << 2,
        Straight = 1 << 3,
        Left = 1 << 4,
        Right = 1 << 5,
        Natural = 1 << 6,
    }
}
