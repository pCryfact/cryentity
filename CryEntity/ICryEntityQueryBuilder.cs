﻿
namespace CryEntity
{
    public interface ICryEntityQueryBuilder
    {
        string CreateSelectQuery(CryEntityQueryStatement createQueryInfo);
        string CreateInsertQuery(CryEntityQueryStatement createQueryInfo);
        string CreateUpdateQuery(CryEntityQueryStatement createQueryInfo);
        string CreateGetLastInsertedIdQuery();
        string CreateDeleteQuery(CryEntityQueryStatement createQueryInfo);
    }
}
