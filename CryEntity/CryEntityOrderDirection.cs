﻿
namespace CryEntity
{
    public enum CryEntityOrderDirection
    {
        Default,
        Ascending,
        Descending,
    }
}
