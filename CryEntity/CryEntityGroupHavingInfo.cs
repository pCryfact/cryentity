﻿using System.Collections.Generic;

namespace CryEntity
{
    public sealed class CryEntityGroupHavingInfo
    {
        internal List<CryEntityCreateQueryColumnInfo> AdditionalColumns { get; }
        public CryEntityQueryWhereOnInfo Having { get; internal set; }
        public CryEntityGroupInfo GroupInfo { get; internal set; }

        internal CryEntityGroupHavingInfo()
        {
            AdditionalColumns = new List<CryEntityCreateQueryColumnInfo>();
        }
    }
}
